<?php

namespace App\Mail;

use App\Ulasan;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailable extends Mailable
{
    use Queueable, SerializesModels;
    
    public $ulasan;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($ulasan)
    {
        $this->ulasan = $ulasan;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Notifikasi konfirmasi kontribusi')
            ->from('no-reply@nearyou.com', 'Near You')
            ->view('emails.name');
    }
}
