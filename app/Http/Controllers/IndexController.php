<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mapper;
use App\Ulasan;
use App\Kategori;
use DB;
class IndexController extends Controller
{
    function index() {
    	$data = DB::table('ulasans')
            ->join('kategoris', 'ulasans.kategori_id', '=', 'kategoris.id')
            ->select('kategoris.*','ulasans.*')
            ->get();
        Mapper::map(-7.005293, 110.430797, ['center' => false, 'zoom' => 4,]);
        foreach ($data as $key => $value) {
	        Mapper::marker($value->tempat_latitude, $value->tempat_longitude, [
	        	'markers' => [
	        		'symbol' => 'circle',
	        		'icon' => 'https://images.vexels.com/media/users/3/141120/isolated/preview/a5ff757d7423e6c757795e7b60183180-rocket-round-icon-by-vexels.png'
	        	]
	        ]);
        }
        $kategori = DB::table('kategoris')
                    ->select('kategoris.*')
                    ->get();
    	return view('welcome', compact('data', 'kategori'));
    }
}
