<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use File;
use App\Kategori;
use DB;
use Illuminate\Http\Request;
use Storage;

class KategoriController extends Controller
{
        public function __construct()
    {
        $this->middleware('auth');
    }
    /*fungsi tambah*/
    public function add ()
    {
        return view('kategori.create');
    }

    /*fungsi simpan*/
    public function save(Request $request)
    {
            $this->validate($request, [
                'kategori_nama' => 'required|unique:kategoris',
                'kategori_icon' => 'required'
                ]);

        $data = new Kategori;
        
        $data->kategori_nama = $request->kategori_nama;
        $data->kategori_icon = 'a';
        $data->save();        
        
        $data1=Kategori::find($data->id);
        $ext = Input::file('kategori_icon')->getClientOriginalExtension();
        $filename = 'Kategori' . $data->id. '.' . $ext;
        $request->file('kategori_icon')->move('icon_kategori', $filename);
        $data1->kategori_icon=$filename;
        $data1->save();

        return redirect('/kategori');
    }

    /*fungsi index*/
    public function index()
    {
    	$data = Kategori::all();
    	// return response()->json($data);
        return view('kategori.index')->with('kategori',$data);
    }

    /*fungsi edit*/
    public function edit($id)
    {
        $data = Kategori::find($id);

        return view('kategori.edit')->with('kategori', $data);
    }

    /*fungsi edit*/
    public function update(Request $request, $id)
    {
        $kategori = Kategori::find($id);


        $kategori->kategori_nama = $request->kategori_nama;

        if ($request->file('kategori_icon')) {
          File::delete('icon_kategori/' . $kategori->kategori_icon);

        $ext = Input::file('kategori_icon')->getClientOriginalExtension();
        $filename = 'Kategori' . $kategori->id. '.' . $ext;
        $request->file('kategori_icon')->move('icon_kategori', $filename);
      }


        $kategori->save();

      return redirect('kategori/');
    }
    public function destroy($id)
    {
        $data = Kategori::find($id);
        $data->delete();
        return redirect(url()->previous());
    }
}
