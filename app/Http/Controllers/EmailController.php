<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;

class EmailController extends Controller
{
    public function sendRecomenNotif(Request $request){
       $name = 'Nisa Ulfa';
	   Mail::to('nisaulfasaida@gmail.com')->send(new SendMailable($name));
	   
	   return 'Email was sent'; 
	}
	public function sendRepostNotif(Request $request){
       $name = 'Nisa Ulfa';
	   Mail::to('nisaulfasaida@gmail.com')->send(new SendMailable($name));
	   
	   return 'Email was sent'; 
	}
}
