<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;use File;
use App\Ulasan;
use App\Pengunjung;
use App\Kategori;
use App\Gambar;
use App\Laporan;
use DB;
use Mapper;
use Illuminate\Http\Request;
use Storage;

class LaporController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['addNewReport','saveNewReport']]);
    }
        public function verifIndex()
    {
    	$data = DB::table('laporans')
            ->join('ulasans', 'laporans.tempat_id', '=', 'ulasans.id')
            ->join('pengunjungs', 'laporans.id', '=', 'pengunjungs.ket_id')
            ->where('pengunjungs.keterangan','=','Laporan')
            ->where('laporan_status','=','Belum Terverifikasi')
            ->select('laporans.*', 'ulasans.tempat_nama', 'pengunjungs.pengunjung_nama', 'pengunjungs.pengunjung_email', 'pengunjungs.keterangan')
            ->get();
    	return view('laporan.verif_index')->with('laporan',$data);
    }
        public function verifShow($id)
    {
        $data1 = DB::table('laporans')
            ->join('ulasans', 'laporans.tempat_id', '=', 'ulasans.id')
            ->join('pengunjungs', 'laporans.id', '=', 'pengunjungs.ket_id')
            ->where('pengunjungs.keterangan','=','Laporan')
            ->where('laporans.id', '=', $id)
            ->select('laporans.*', 'ulasans.tempat_nama', 'pengunjungs.*')
            ->first();
        return view('laporan.verif_show')
        ->with('laporan',$data1);
    }
    public function verifDestroy($id)
    {
        $data = Laporan::find($id);
        $data->delete();
        $data1 = DB::table('pengunjungs')
            ->where('pengunjungs.keterangan','=','Laporan')
            ->where('ket_id','=', $id)
            ->delete();
        return redirect ('/verifikasi_laporan');
    }
    public function verifikasi ($id)
    {
        $data1 = Laporan::find($id);
        $data1->laporan_status = 'Terverifikasi';
        $data1->save();

        $data2 = DB::table('laporans')
            ->join('pengunjungs', 'laporans.id', '=', 'pengunjungs.ket_id')
            ->join('ulasans', 'laporans.tempat_id', '=', 'ulasans.id')
            ->where('pengunjungs.keterangan','=','Laporan')
            ->where('laporans.id','=', $id)
            ->select('laporans.*','pengunjungs.pengunjung_nama','pengunjungs.pengunjung_email','pengunjungs.keterangan', 'ulasans.*')
            ->first();

        $ulasan = $data2;
        Mail::to('nisaulfasaida@gmail.com')->send(new SendMailable($ulasan));

        return redirect ('/verifikasi_laporan');
    }
    public function index()
    {
        $data = DB::table('laporans')
            ->join('ulasans', 'laporans.tempat_id', '=', 'ulasans.id')
            ->join('pengunjungs', 'laporans.id', '=', 'pengunjungs.ket_id')
            ->where('pengunjungs.keterangan','=','Laporan')
            ->select('laporans.*', 'ulasans.tempat_nama', 'pengunjungs.*')
            ->get();
        return view('laporan.index')
            ->with('laporan',$data);
    }
    public function show($id)
    {
        $data = DB::table('laporans')
            ->join('ulasans', 'laporans.tempat_id', '=', 'ulasans.id')
            ->join('pengunjungs', 'laporans.id', '=', 'pengunjungs.ket_id')
            ->where('pengunjungs.keterangan','=','Laporan')
            ->where('laporans.id', '=', $id)
            ->select('laporans.*', 'ulasans.tempat_nama', 'pengunjungs.pengunjung_nama', 'pengunjungs.pengunjung_email')
            ->first();
        return view('laporan.show')
                ->with('laporan',$data);
    }
    public function destroy($id)
    {
        $data = Laporan::find($id);
        $data->delete();
        $data1 = DB::table('pengunjungs')
            ->where('pengunjungs.keterangan','=','Laporan')
            ->where('ket_id','=', $id)
            ->delete();
        return redirect ('/laporan');
    }
    // visitor
        public function addNewReport($id) {
        $data = Ulasan::find($id);

        return view('visitor.laporCreate', compact('data'));
    }
        public function saveNewReport($id, Request $request)
    {
        $this->validate($request, [
            'pengunjung_nama' => 'required|max:255|string',
            'pengunjung_email' => 'required|email',
            'laporan_deskripsi' => 'required|string'
        ]);

        $tempat_id =$id;
        $data1 = new Laporan;
        $data1->laporan_deskripsi = $request->laporan_deskripsi;
        $data1->tempat_id = $id;
        $data1->laporan_status = 'Belum Terverifikasi';
        $data1->save();

        $data2 = new Pengunjung;
        $data2->ket_id= $data1->id;
        $data2->pengunjung_nama = $request->pengunjung_nama;
        $data2->pengunjung_email = $request->pengunjung_email;
        $data2->keterangan = "Laporan";
        $data2->save();

        $data = Ulasan::all();
        // return redirect('/detail/$id', compact('data'));
        // return redirect()->route('/detail/$tempat_id', ['tempat_id' => $id], compact('data'));
        return redirect ('/peta');
    }
}
