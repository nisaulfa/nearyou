<?php

namespace App\Http\Controllers;
use App\Ulasan;
use App\Gambar;
use DB;

use Illuminate\Http\Request;

class PetaController extends Controller
{

    public function index() {
    	$kategori = DB::table('kategoris')
                    ->select('kategoris.*')
                    ->get();
    	return view('/visitor.peta', compact('kategori'));
    }

    public function haversine($data, $radius, $category){
    	$location = (explode('_',$data,2));
	    $data = DB::table('ulasans')
		    ->where('ulasans.kategori_id', $category)
	        ->select('ulasans.*')
	        ->selectRaw('( 3959 * acos( cos( radians(?) ) *
	                           cos( radians( tempat_latitude ) )
	                           * cos( radians( tempat_longitude ) - radians(?)
	                           ) + sin( radians(?) ) *
	                           sin( radians( tempat_latitude ) ) )
	                         ) AS distance', [$location[0], $location[1], $location[0]])
	        ->havingRaw("distance < ?", [$radius])
	        // ->group_by('ulasans.id');
	        ->get();
	        // return response()->json($data);
	    return view('/visitor.result')->with('result',$data);
	}

}
