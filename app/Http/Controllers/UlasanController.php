<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use File;
use App\Ulasan;
use App\Pengunjung;
use App\Kategori;
use App\Gambar;
use DB;
use Mapper;
use Illuminate\Http\Request;
use Storage;

class UlasanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['addNewPlace','saveNewPlace','showDetail']]);
    }
    public function index()
    {
        $data = DB::table('ulasans')
            ->join('kategoris', 'ulasans.kategori_id', '=', 'kategoris.id')
            ->select('kategoris.*','ulasans.*')
            ->get();
        return view('ulasan.index')
            ->with('ulasan',$data);
    }
    // public function addMaps()
    // {
    //     return view('ulasan.petaCreate');
    // }
    public function add()
    {
        $data = Kategori::all();
        return view('ulasan.create')->with('kategori',$data);
    }

    public function save(Request $request)
    {

    	$data = new Ulasan;
        $data->tempat_nama = $request->tempat_nama;
        $data->tempat_deskripsi = $request->tempat_deskripsi;
        $data->tempat_telepon = $request->tempat_telepon;
        $data->tempat_alamat = $request->tempat_alamat;
        $data->kategori_id = $request->kategori_id;
        $data->tempat_latitude = $request->tempat_latitude;
        $data->tempat_longitude = $request->tempat_longitude;
        $data->tempat_status = $request->tempat_status;
        $data->save();
        // gambar
        $files = Input::file('gambar_direktori');
        $key=1;
        foreach ($files as $file) {
            $data1 = new Gambar;
            $data1->tempat_id = $data->id;
            $ext = $file->getClientOriginalExtension();
            $filename = 'Gambar' . $data->id.'.'.$key. '.' . $ext;
            $file->move('gambar_ulasan', $filename);
            $key=$key+1;
            $data1->gambar_direktori = $filename;
            $data1->save();
        }
        //pengunjung
        $data2 = new Pengunjung;
        $data2->ket_id= $data->id;
        $data2->pengunjung_nama = 'Admin';
        $data2->pengunjung_email = 'Admin';
        $data2->keterangan = "Ulasan";
        $data2->save();
        return redirect('/ulasan');
    }
    public function verifIndex()
    {
        $data = DB::table('ulasans')
            ->join('kategoris', 'ulasans.kategori_id', '=', 'kategoris.id')
            ->join('pengunjungs', 'ulasans.id', '=', 'pengunjungs.ket_id')
            ->where('pengunjungs.keterangan','=','Ulasan')
            ->where('tempat_status', '=', 'Belum Terverifikasi')
            ->select('ulasans.*', 'kategoris.kategori_nama', 'pengunjungs.pengunjung_nama')
            ->get();
        return view('ulasan.verif_index')->with('ulasan',$data);
    }
    public function verifShow($id)
    {
        $data = Ulasan::find($id);
        $data1 = DB::table('ulasans')
            ->join('pengunjungs', 'ulasans.id', '=', 'pengunjungs.ket_id')
            ->join('gambars', 'ulasans.id', '=', 'gambars.tempat_id')
            ->join('kategoris', 'ulasans.kategori_id', '=', 'kategoris.id')
            ->where('pengunjungs.keterangan','=','Ulasan')
            ->where('ulasans.id', '=', $id)
            ->select('gambars.*','ulasans.*', 'pengunjungs.*','kategoris.kategori_nama')
            // ->select('gambars.*','ulasans.*')
            ->get();
        $lat = $data->tempat_latitude;
        $lng = $data->tempat_longitude;
        Mapper::map($lat, $lng);   
        return view('ulasan.verif_show')
        ->with('ulasan',$data)
        ->with('data',$data1);

    }
    public function show($id)
    {
        $data = Ulasan::find($id);
        $data1 = DB::table('ulasans')
            ->join('pengunjungs', 'ulasans.id', '=', 'pengunjungs.ket_id')
            ->join('gambars', 'ulasans.id', '=', 'gambars.tempat_id')
            ->join('kategoris', 'ulasans.kategori_id', '=', 'kategoris.id')
            ->where('pengunjungs.keterangan','=','Ulasan')
            ->where('ulasans.id', '=', $id)
            ->select('gambars.*','ulasans.*', 'pengunjungs.*','kategoris.kategori_nama')
            // ->select('gambars.*','ulasans.*')
            ->get();
        $lat = $data->tempat_latitude;
        $lng = $data->tempat_longitude;
        Mapper::map($lat, $lng);
        return view('ulasan.show')
                ->with('ulasan',$data)
                ->with('data',$data1);
    }
    public function edit($id)
    {
        $data2 = DB::table('ulasans')
            ->join('kategoris', 'ulasans.kategori_id', '=', 'kategoris.id')
            ->where('ulasans.id', '=', $id)
            ->select('ulasans.*','kategoris.kategori_nama')
            ->first();
        $data1 = Kategori::all();
        $data = DB::table('ulasans')
            ->join('gambars', 'ulasans.id', '=', 'gambars.tempat_id')
            ->join('kategoris', 'ulasans.kategori_id', '=', 'kategoris.id')
            ->join('pengunjungs', 'ulasans.id', '=', 'pengunjungs.ket_id')
            ->where('pengunjungs.keterangan','=','Ulasan')
            ->where('ulasans.id', '=', $id)
            ->select('gambars.gambar_direktori','ulasans.*','kategoris.kategori_nama')
            ->select('gambars.gambar_direktori','ulasans.*','kategoris.kategori_nama','pengunjungs.pengunjung_nama','pengunjungs.pengunjung_email')
            ->first();
        return view('ulasan.edit')
            ->with('ulasan',$data)
            ->with('kategori', $data1)
            ->with('ulas', $data2);
    }
    public function update (Request $request, $id)
    {
        $data = Ulasan::find($id);

        $data->tempat_nama = $request->tempat_nama;
        $data->tempat_deskripsi = $request->tempat_deskripsi;
        $data->tempat_telepon = $request->tempat_telepon;
        $data->tempat_alamat = $request->tempat_alamat;
        $data->tempat_latitude = $request->tempat_latitude;
        $data->tempat_longitude = $request->tempat_longitude;
        $data->tempat_status = $request->tempat_status;
        $data->kategori_id = $request->kategori_id;
        $data->save();
        //gambar
        $data1 = new Gambar;
        $data1->tempat_id = $data->id;

        if($request->file('gambar_direktori')) {
            File::delete('gambar_ulasan/' . $data->gambar_direktori);

        $ext = Input::file('gambar_direktori')->getClientOriginalExtension();
        $filename = 'Gambar' . $data->id. '.' . $ext;
        $request->file('gambar_direktori')->move('gambar_ulasan', $filename);
        
        $data1->gambar_direktori = $filename;
        $data1->save();
        }
        //nama pengunjung tidak bisa diubah
        return redirect('/ulasan');
    }
    public function destroy($id)
    {
        $data = Ulasan::find($id);
        $data->delete();

        $data1 = Gambar::where('tempat_id','=',$id);
        $data1->delete();

        $data2 = DB::table('pengunjungs')
            ->where('pengunjungs.keterangan','=','Ulasan')
            ->where('ket_id','=', $id)
            ->delete();
        return redirect ('/ulasan');
    }
    public function verifDestroy($id)
    {
        $data = Ulasan::find($id);
        $data->delete();

        $data1 = new Gambar;
        $data1->tempat_id = $data->id;
        $data1->delete();

        $data2 = DB::table('pengunjungs')
            ->where('pengunjungs.keterangan','=','Ulasan')
            ->where('ket_id','=', $id)
            ->delete();

        return redirect ('/verifikasi_ulasan');
    }
        public function addNewPlace()
    {
        $data = Kategori::all();
        return view('visitor.create')->with('kategori',$data);
    }
        public function saveNewPlace(Request $request)
    {
        $this->validate($request,['tempat_nama' => 'required',
            'tempat_deskripsi' => 'required',
            'kategori_id' => 'required',
            'tempat_alamat' => 'required',
            'tempat_telepon'=> 'required',
            'tempat_latitude' => 'required',
            'tempat_longitude' => 'required']);
        $data = new Ulasan;
        $data->tempat_nama = $request->tempat_nama;
        $data->tempat_deskripsi = $request->tempat_deskripsi;
        $data->kategori_id = $request->kategori_id;
        $data->tempat_alamat = $request->tempat_alamat;
        $data->tempat_telepon = $request->tempat_telepon;
        $data->tempat_latitude = $request->tempat_latitude;
        $data->tempat_longitude = $request->tempat_longitude;
        $data->tempat_status = 'Belum Terverifikasi';
        $data->save();
        $files = Input::file('gambar_direktori');
        $key=1;

        foreach ($files as $file) {
            $data1 = new Gambar;
            $data1->tempat_id = $data->id;
            $ext = $file->getClientOriginalExtension();
            $filename = 'Gambar' . $data->id.'.'.$key. '.' . $ext;
            $file->move('gambar_ulasan', $filename);
            $key=$key+1;
            $data1->gambar_direktori = $filename;
            $data1->save();
        }

        $data2 = new Pengunjung;
        $data2->ket_id= $data->id;
        $data2->pengunjung_nama = $request->pengunjung_nama;
        $data2->pengunjung_email = $request->pengunjung_email;
        $data2->keterangan = 'Ulasan';
        $data2->save();

        return redirect('/');
    }
    /*Verifikasi*/
    public function verifikasi ($id)
    {
        $data1 = Ulasan::find($id);
        $data1->tempat_status = 'Terverifikasi';
        $data1->save();

        $data2 = DB::table('ulasans')
            ->join('pengunjungs', 'ulasans.id', '=', 'pengunjungs.ket_id')
            ->where('pengunjungs.keterangan','=','Ulasan')
            ->where('ulasans.id','=', $id)
            ->select('ulasans.*','pengunjungs.pengunjung_nama','pengunjungs.pengunjung_email','pengunjungs.keterangan')
            ->first();

        $ulasan = $data2;
        Mail::to('nisaulfasaida@gmail.com')->send(new SendMailable($ulasan));
        return redirect ('/verifikasi_ulasan');
    }
    public function showDetail($id) {
        $data = DB::table('ulasans')
            ->join('pengunjungs', 'ulasans.id', '=', 'pengunjungs.ket_id')
            // ->join('gambars', 'ulasans.id', '=', 'gambars.tempat_id')
            ->join('kategoris', 'ulasans.kategori_id', '=', 'kategoris.id')
            ->where('pengunjungs.keterangan','=','Ulasan')
            ->where('ulasans.id', '=', $id)
            ->select('ulasans.*','kategoris.kategori_nama')
            // ->select('gambars.*','ulasans.*')
            ->first();
        $dataGambar = DB::table('ulasans')
                    ->join('gambars', 'ulasans.id', '=', 'gambars.tempat_id')
                    ->where('ulasans.id', '=', $id)
                    ->select('gambars.gambar_direktori')
                    ->get();        
        foreach ($dataGambar as $key => $value) {
            $daftar_gambar[] = $value->gambar_direktori;
        }
        $data->daftar_gambar = $daftar_gambar;
        
        $lat = $data->tempat_latitude;
        $lng = $data->tempat_longitude;
        Mapper::map($lat, $lng);
        return view('visitor.show', compact('data'));
    }
}

