<?php

namespace App\Http\Controllers;
use File;
use App\Ulasan;
use App\Laporan;
use DB;
use Illuminate\Http\Request;
use Storage;
use Mapper;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */ 
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data2 = DB::table('ulasans')
            ->where('tempat_status', '=', 'Belum Terverifikasi')
            ->count();
        $data1 = DB::table('ulasans')->count();
        $data = DB::table('laporans')->count();
        return view('home')
            ->with('laporan',$data)
            ->with('ulasan',$data1)
            ->with('belumverif',$data2);
    }
}
