<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use File;
use App\Ulasan;
use App\Kategori;
use App\Gambar;
use App\Laporan;
use DB;
use Mapper;
use Illuminate\Http\Request;
use Storage;

class AndroidController extends Controller
{
// filterr
    public function filter($data, $radius, $category){
    	$location = (explode('_',$data,2));
	    $data = DB::table('ulasans')
		    ->where('ulasans.kategori_id', $category)
	        ->select('ulasans.*')
	        ->selectRaw('( 3959 * acos( cos( radians(?) ) *
	                           cos( radians( tempat_latitude ) )
	                           * cos( radians( tempat_longitude ) - radians(?)
	                           ) + sin( radians(?) ) *
	                           sin( radians( tempat_latitude ) ) )
	                         ) AS distance', [$location[0], $location[1], $location[0]])
	        ->havingRaw("distance < ?", [$radius])
	        ->get();
	        // return response()->json($data);

        echo json_encode($data);
	    // return view('/visitor.result')->with('result',$data);
	}
// Detail
	public function showDetail($id) {
        $data = DB::table('ulasans')
            ->join('gambars', 'ulasans.id', '=', 'gambars.tempat_id')
            ->join('kategoris', 'ulasans.kategori_id', '=', 'kategoris.id')
            ->where('ulasans.id', '=', $id)
            ->select('gambars.*','ulasans.*')
            ->get();

        echo json_encode($data);
        // $data = Ulasan::find($id);
        // return view('visitor.show', compact('data'));
    }
// Tambah tempat
        public function saveNewPlace(Request $request)
    {

        $data = new Ulasan;
        $data->tempat_nama = $request->tempat_nama;
        $data->tempat_deskripsi = $request->tempat_deskripsi;
        $data->kategori_id = $request->kategori_id;
        $data->tempat_alamat = $request->tempat_alamat;
        $data->tempat_telepon = $request->tempat_telepon;
        $data->tempat_latitude = $request->tempat_latitude;
        $data->tempat_longitude = $request->tempat_longitude;
        $data->tempat_status = 'Belum Terverifikasi';
        $data->save();


        echo json_encode($data);
    }
        public function saveNewReport($id, Request $request)
    {
        $tempat_id =$id;
        $data1 = new Laporan;
        $data1->laporan_nama= $request->laporan_nama;
        $data1->laporan_email = $request->laporan_email;
        $data1->laporan_deskripsi = $request->laporan_deskripsi;
        $data1->tempat_id = $id;
        $data1->laporan_status = 'Belum Terverifikasi';
        $data1->save();

        $data = Ulasan::all();
        echo json_encode($data1);
        // return redirect ('/peta');
    }
        public function getAutoIncre()
    {
    	$data = DB::select('select `AUTO_INCREMENT` from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA = "smartcity" and TABLE_NAME = "ulasans"');
    	echo json_encode($data);
    }





    //     public function saveNewReport($id, Request $request)
    // {
    //     $tempat_id =$id;
    //     $data1 = new Laporan;
    //     $data1->laporan_nama= '$request->laporan_nama';
    //     $data1->laporan_email = '$request->laporan_email';
    //     $data1->laporan_deskripsi = '$request->laporan_deskripsi';
    //     $data1->tempat_id = $id;
    //     $data1->laporan_status = 'Belum Terverifikasi';
    //     $data1->save();

    //     echo json_encode($data1);
    //     // return redirect ('/peta');
    // }


    //     public function saveNewPlace(Request $request)
    // {
    // 	$data = new Ulasan;
    // 	$data->id = 7;
    //     $data->tempat_nama = '$request->tempat_nama';
    //     $data->tempat_deskripsi = '$request->tempat_deskripsi';
    //     $data->kategori_id = 2;
    //     $data->tempat_alamat = '$request->tempat_alamat';
    //     $data->tempat_telepon = '676';
    //     $data->tempat_latitude = '787';
    //     $data->tempat_longitude = '768';
    //     $data->tempat_status = 'Belum Terverifikasi';
    //     $data->save();

    //     if (!$saved) {
    //     	echo 'gagal';

    //     }else{ echo 'sukses';}

    //     echo json_encode($data);
    //     // return redirect('/');
    // }
    // public function add_photo (Request $request)
    // {
    //     $files = Input::file('gambar_direktori');
    //     $key=1;

    //         $data1 = new Gambar;
    //         $data1->tempat_id = $data->id;
    //         $ext = $file->getClientOriginalExtension();
    //         $filename = 'Gambar' . $data->id.'.'.$key. '.' . $ext;
    //         $file->move('gambar_ulasan', $filename);
    //         $key=$key+1;
    //         $data1->gambar_direktori = $filename;
    //         $data1->gambar_status = 'draf';   
    //         $data1->save();
    //     echo json_encode($data1);
    // }

}
