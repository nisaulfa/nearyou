<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
// 	$kategori = \App\Kategori::all();
// 	$ulasan = \App\Ulasan::all();
//     return view('welcome')
//     	->with('kategori', $kategori)
//     	->with('ulasan', $ulasan);
Route::get('/peta','petaController@index');
Route::get('/haversine/{data}/{radius}/{category}','petaController@haversine');
Route::get('/detail/{id}','ulasanController@showDetail');
Route::get('/lapor_tempat/{id}','laporController@addNewReport');
Route::post('/lapor_tempat/{id}','laporController@saveNewReport')->name('simpan_laporan');

Route::get('/', 'IndexController@index');
	// $kategori = \App\Kategori::all();
 //    return view('/')p
 //    	->with('kategori', $kategori);
Route::get('/test', function () {
	$kategori = \App\Kategori::all();
    return view('coba_welcome')
    	->with('kategori', $kategori);
});
 
Auth::routes();

/*Home*/
Route::get('/home', 'HomeController@index')->name('home');

/*Ulasan*/
Route::get('/ulasan', 'UlasanController@index');
Route::get('/ulasan/{id}', 'UlasanController@show');
Route::get('/ulasan/{id}/hapus', 'UlasanController@destroy');
Route::get('/tambah_ulasan', 'UlasanController@add');
Route::post('/tambah_ulasan', 'UlasanController@save');
Route::get('/ulasan/{id}/edit', 'UlasanController@edit');
Route::post('/ulasan/{id}/edit', 'UlasanController@update');
Route::get('/tambah_petaUlasan', 'UlasanController@addMaps');
Route::get('/verifikasi_ulasan', 'UlasanController@verifIndex');
Route::get('/verifikasi_ulasan/{id}', 'UlasanController@verifShow');
Route::get('/verifikasi_ulasan/{id}/hapus', 'UlasanController@verifDestroy');
Route::get('/verifikasi_ulasan/verifikasi/{id}', 'UlasanController@verifikasi');
/*Halaman Pengunjung*/
Route::get('/tambah_tempat', 'UlasanController@addNewPlace');
Route::post('/simpan_rekomendasi', 'UlasanController@saveNewPlace')->name('simpan_rekomendasi');


/*Kategori*/
Route::get('/kategori', 'KategoriController@index');
Route::get('/tambah_kategori', 'KategoriController@add');
Route::post('/tambah_kategori', 'KategoriController@save')->name('tambah_kategori');
Route::get('/kategori/{id}/edit', 'KategoriController@edit');
Route::post('/kategori/{id}/edit', 'KategoriController@update');
Route::get('/kategori/{id}/hapus', 'KategoriController@destroy');


/*Laporan*/
Route::get('/laporan', 'LaporController@index');
Route::get('/laporan/{id}', 'LaporController@show');
Route::get('/laporan/{id}/hapus', 'LaporController@destroy');
Route::get('/verifikasi_laporan', 'LaporController@verifIndex');
Route::get('/verifikasi_laporan/{id}', 'LaporController@verifShow');
Route::get('/verifikasi_laporan/{id}/hapus', 'LaporController@verifDestroy');
Route::get('/verifikasi_laporan/verifikasi/{id}', 'LaporController@verifikasi');

/*Peta*/
Route::get('/peta_update', 'petaController@update');

// Android
Route::get('/filter/{data}/{radius}/{category}','androidController@filter');
Route::get('show/{id}', 'androidController@showDetail');
Route::post('/new_place', 'androidController@saveNewPlace');
Route::get('/getAI', 'androidController@getAutoIncre');
Route::post('/add_photo/{id}','androidController@add_photo');
Route::post('/add_report/{id}','androidController@saveNewReport');

// Email
Route::get('/send/email', 'EmailController@sendRecomenNotif');

// Logout emaiil
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
// Mailtrap info
// password : (mailtrap) nisamailtrap

// Route::get('test_email', function(){
// 	Mail::raw('Sending emails with Mailgun and Laravel is easy!', function($message)
// 	{
// 		$message->subject('Mailgun and Laravel are awesome!');
// 		$message->from('no-reply@nisaulfasaida.com', 'Website Name');
// 		$message->to('nisaulfasaida@gmail.com');
// 	});
// });
