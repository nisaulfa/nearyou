@extends('layouts.visitor')
@section('contentFronfEnd')

    
     <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">NEAR YOU</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav text-uppercase ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#services">Peta</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ url('/tambah_tempat')}}">Tambah Tempat Baru</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- Services -->
    <section id="services">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Peta</h2>
            <h3 class="section-subheading text-muted">Rekomendasi Tempat terdekar Anda dalam jarak 5km</h3>
              <div id="floating-panel">
                <input id="address" type="textbox" value="Sydney, NSW">
                <input id="submit" type="button" value="Geocode">
              </div>
              <select class="form-control" id="category">
                <option value="0">-Pilih Kategori-</option>
                @foreach($kategori as $data)
                  <option value="{{ $data->id }}">{{ $data->kategori_nama }}</option>
                @endforeach
              </select>
              <input type="range" min="0" max="10" value="5" id="radius" style="width: 100%; margin: 20px 0;">
              <h3 style="margin-bottom: 40px;">Radius : <span id="radius_value"></span></h3>
              <div id="map"></div>
          </div>
        </div>
      </div>
    </section>

   <script>
      var slider = document.getElementById("radius");
      var output = document.getElementById("radius_value");
      
      output.innerHTML = slider.value;
      slider.oninput = function() {
        output.innerHTML = this.value;
      }

      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 10,
          center: {lat: -34.397, lng: 150.644}
        });
        var geocoder = new google.maps.Geocoder();

        document.getElementById('submit').addEventListener('click', function() {
          geocodeAddress(geocoder, map);
        });
      }

      function geocodeAddress(geocoder, resultsMap) {
        var address = document.getElementById('address').value;
        var category = document.getElementById('category');

        geocoder.geocode({'address': address}, function(results, status) {
          if (status === 'OK') {
            var location = results[0].geometry.location.lat() + "_" + results[0].geometry.location.lng();
            var radius = document.getElementById("radius").value;
            var category_value = category.options[category.selectedIndex].value;

            {{-- var url = '{{ url("haversine", ":location", ":radius") }}'; --}}
            // url = url.replace('%3Alocation', location).replace('%3Araidus', radius);
            var url = '{{ url('haversine')}}';
            var new_url = url + '/' + location + '/' + radius + '/' + category_value
            window.location.href = new_url

          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
      }
    </script>


    {{-- <script type="text/javascript">
      data.forEach(value => {
        console.log('value',value.kategori_nama)
      })
    </script>

 --}}
@endsection