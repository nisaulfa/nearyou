@extends('layouts.visitor')
@section('contentFronfEnd')
<!-- Contact -->
<section id="service">
<div class="container">
  <div class="row">
    <div class="col-12 text-center">
      <h2 class="section-heading text-uppercase">Laporkan Tempat</h2>
    </div>
  </div>
  <div class="row justify-content-md-center">
    <div class="col-8">
      <form action="{{url()->current()}}" method="post" class="form-horizontal" enctype="multipart/form-data">
        {{csrf_field()}}
      <!-- nama tempat -->
        <div class="form-group" {{ $errors->has('pengunjung_nama') ? 'has-error' : '' }}>
          <div class="row">
            <div class="col-3"><label for="text-input" class=" form-control-label-vistor">Nama Pelapor</label></div>
            <div class="col-9"><input type="text" id="text-input" name="pengunjung_nama" placeholder="Text" class="form-control"><small class="form-text text-muted">Contoh: Nisa Ulfa Saida</small>
            {!! $errors->first('pengunjung_nama', '<p class="help-block">:message</p>') !!}
            </div>
          </div>
        </div>
        {{-- email --}}                       
        <div class="form-group" {{ $errors->has('pengunjung_email') ? 'has-error' : '' }}>
          <div class="row">
            <div class="col-3"><label for="text-input" class=" form-control-label-vistor">E-mail Pelapor</label></div>
            <div class="col-9"><input type="email" id="email" name="pengunjung_email" placeholder="E-mail Pelapor" class="form-control">
            {!! $errors->first('pengunjung_email', '<p class="help-block">:message</p>') !!}
            </div>
          </div>
        </div>
        <!-- deskripsi -->
        <div class="form-group" {{ $errors->has('laporan_deskripsi') ? 'has-error' : '' }}>
          <div class="row">
            <div class="col-3"><label for="textarea-input" class=" form-control-label-vistor">Deskripsi Laporan</label></div>
            <div class="col-9"><textarea name="laporan_deskripsi" id="textarea-input" rows="9" placeholder="Deskripsi" class="form-control"></textarea>
            {!! $errors->first('laporan_deskripsi', '<p class="help-block">:message</p>') !!}
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="row">
            <div class="col-3"></div>
            <div class="col-9">
              <button type="submit" class="btn btn-success">
                <i class="fa fa-dot-circle-o"></i> Submit
              </button>
              <a class="btn btn-secondary" style="color: #fff !important;" href="{{url('detail', $data->id)}}">
                <i class="fa fa-outline"></i> Batal
              </a>
            </div>
          </div>
        </div>
      </form>
     </div>
  </div>
</div>
</section>
@endsection