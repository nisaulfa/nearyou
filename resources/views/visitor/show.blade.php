@extends('layouts.visitor')
@section('contentFronfEnd')

    <!-- Services -->
    <section id="service">
      <div class="container">
        <div class="row">
          <div class="col-12 text-center">
            <h3 class="section-heading text-uppercase" style="margin-bottom: 20px;">Detail Tempat "{{$data->tempat_nama}}"</h3>
            {{-- <h3 class="section-subheading text-muted">Hasil rekomendasi</h3> --}}
          </div>
          <div class="col-6">
            <div style="width: 100%; height: 700px;">
            {!! Mapper::render() !!}
            </div>
          </div>
          <div class="col-6 col-offset-3">
            <div class="card">
              <div style="width:100%">
                  @foreach($data->daftar_gambar as $gambar)
                      <img class="mySlides" style="padding: 0;margin: -10; height: 300px; object-fit: cover;" src="{{asset('gambar_ulasan/'.$gambar)}}" width="100%">
                  @endforeach
                  <div class="d-flex mb-4" style="position: relative; background-color: #ddd;">
                    <div class="p-2 bd-highlight" onclick="plusDivs(-1)"><span class="btn btn-secondary">&#10094;</span></div>
                    <div class="ml-auto p-2 bd-highlight" onclick="plusDivs(1)"><span class="btn btn-secondary">&#10095;</span></div>
                  </div>
              </div>
              <div class="card-body">
                <small class="text-muted">Nama tempat</small>
                <h3 style="margin-top: 0px;">{{$data->tempat_nama}}</h3>
                <small class="text-muted">Deskripsi</small>
                <p>{{$data->tempat_deskripsi}}</p>
                <small class="text-muted">Nomor telepon</small>
                <p>{{$data->tempat_telepon}}</p>
                <small class="text-muted">Alamat</small>
                <p>{{$data->tempat_alamat}}</p>
              </div>
              <div class="card-footer">
                <a href="{{url('lapor_tempat', $data->id)}}" class="btn btn-danger">Laporkan Tempat</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>


<script>
function initMap() {
  var sites = {!!json_encode($data)!!};
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 17,
    center: {lat: sites.tempat_latitude, lng: sites.tempat_longitude}
  });
  var marker = new google.maps.Marker({
      position: {lat: sites.tempat_latitude, lng: sites.tempat_longitude},
      title:"Hello World!",
      map: map
  });
}
</script>

<script>
var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
     dots[i].className = dots[i].className.replace(" w3-white", "");
  }
  x[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " w3-white";
}

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>

@endsection