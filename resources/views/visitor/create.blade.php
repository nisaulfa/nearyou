@extends('layouts.visitor')
@section('contentFronfEnd')
    <!-- Contact -->
    <section id="contact">
      <div class="container">
        <div class="row">
          <div class="col-12 text-center">
            <h2 class="section-heading text-uppercase">Rekomendasi Tempat Baru</h2>
            <h3 class="section-subheading text-muted subtitle-visitor">Kontribusi Anda sangat berarti bagi kami</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-2"></div>
          <div class="col-8">
            <form action="{{route('simpan_rekomendasi')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
              {{csrf_field()}}
              {{-- nama --}}
              <div class="form-group" {{ $errors->has('pengunjung_nama') ? 'has-error' : '' }}>
                <div class="row">
                  <div class="col-3"><label for="text-input" class="form-control-label">Nama Pengirim</label></div>
                  <div class="col-9"><input type="text" id="text-input" name="pengunjung_nama" placeholder="Text" class="form-control">
                  {!! $errors->first('pengunjung_nama', '<p class="help-block">:message</p>') !!}
                  </div>
                </div>
              </div>
              {{-- email --}}
              <div class="form-group" {{ $errors->has('pengunjung_email') ? 'has-error' : '' }}>
                <div class="row">
                  <div class="col-3"><label for="text-input" class="form-control-label">E-mail Pengirim</label></div>
                  <div class="col-9"><input type="email" id="email" name="pengunjung_email" placeholder="E-mail" class="form-control">
                  {!! $errors->first('pengunjung_email', '<p class="help-block">:message</p>') !!}
                  </div>
                </div>
              </div>
              {{-- nama tempat --}}
              <div class="form-group" {{ $errors->has('tempat_nama') ? 'has-error' : '' }}>
                <div class="row">
                  <div class="col-3"><label for="text-input" class="form-control-label">Nama Tempat</label></div>
                  <div class="col-9"><input type="text" id="text-input" name="tempat_nama" placeholder="Text" class="form-control"><small class="form-text text-muted info-update-map">ex. RM Padang Murah</small>
                  {!! $errors->first('tempat_nama', '<p class="help-block">:message</p>') !!}
                  </div>
                </div>
              </div>
              {{-- deskripsi --}}
              <div class="form-group" {{ $errors->has('tempat_deskripsi') ? 'has-error' : '' }}>
                <div class="row">
                  <div class="col-3"><label for="textarea-input" class="form-control-label">Deskripsi Tempat</label></div>
                  <div class="col-9"><textarea name="tempat_deskripsi" id="textarea-input" rows="9" placeholder="Deskripsi" class="form-control"></textarea>
                  {!! $errors->first('tempat_deskripsi', '<p class="help-block">:message</p>') !!}
                  </div>
                </div>
              </div>
              {{-- kategori --}}
              <div  class="form-group" {{ $errors->has('kategori_id') ? 'has-error' : '' }}>
                <div class="row">
                  <div class="col-3"><label for="text-input" class="form-control-label">Kategori</label></div>
                  <div class="col-9">
                    <select class="form-control" name="kategori_id">
                        <option value="">-Pilih Kategori-</option>
                        @foreach($kategori as $data)
                        <option value="{{ $data->id }}">{{ $data->kategori_nama }}</option>
                        @endforeach
                    </select>
                    {!! $errors->first('kategori_id', '<p class="help-block">:message</p>') !!}
                  </div>
                </div>
              </div>
              {{-- upload image --}}
              <div class="form-group" {{ $errors->has('gambar_direktori') ? 'has-error' : '' }}>
                <div class="row">
                  <div class="col-3"><label for="file-input" class=" form-control-label">Masukkan Foto Tempat</label></div>
                  <div class="col-9">
                    <div class="input-group-btn">
                      <!-- image-preview-input -->
                      <div class="form-control">
                        <input type="file" accept="image/png, image/jpeg, image/gif" name="gambar_direktori[]"/ multiple=""> <!-- rename it -->
                      </div>
                    </div>
                    {!! $errors->first('gambar_direktori', '<p class="help-block">:message</p>') !!}
                  </div>
                </div>
              </div>
            {{-- status --}}
            <div class="form-group" style="margin-top: 23px" >
            <fieldset class="gllpLatlonPicker">
              {{-- nomor telepon --}}
              <div class="form-group" {{ $errors->has('tempat_telepon') ? 'has-error' : '' }}>
                <div class="row">
                  <div class="col-3"><label for="text-input" class=" form-control-label">Nomor Telepon</label></div>
                  <div class="col-9"><input type="text" id="text-input" name="tempat_telepon" placeholder="Text" class="form-control">
                  {!! $errors->first('tempat_telepon', '<p class="help-block">:message</p>') !!}
                  </div>
                </div>
              </div>
              {{-- address --}}
              <div class="form-group" {{ $errors->has('tempat_alamat') ? 'has-error' : '' }}>
                <div class="row">
                  <div class="col-3"><label for="text-input" class=" form-control-label">Alamat</label></div>
                  <div class="col-9"><input type="text" id="text-input" name="tempat_alamat" placeholder="Text" class="form-control">
                  {!! $errors->first('tempat_alamat', '<p class="help-block">:message</p>') !!}
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-3"><label for="text-input" class=" form-control-label">Pilih lokasi</label></div>
                  <div class="col-9">
                    <div class="gllpMap">Google Maps</div>
                  </div>
                </div>
              </div>
              {{-- latitude --}}
              <div class="form-group" style="display: none"{{ $errors->has('tempat_latitude') ? 'has-error' : '' }}>
                <div class="row">
                  <div class="col-3"><label for="text-input" class="form-control-label">Latitude</label></div>
                  <div class="col-9"><input type=
                    "double" id="number-input" name="tempat_latitude" placeholder="" class="gllpLatitude form-control" value="20" ><small class="form-text text-muted"></small>
                  {!! $errors->first('tempat_latitude', '<p class="help-block">:message</p>') !!}
                  </div>
                </div>  
              </div>
              {{-- longitude --}}
              <div class="form-group" style="display: none" {{ $errors->has('tempat_longitude') ? 'has-error' : '' }}>
                <div class="row">
                  <div class="col-3"><label for="text-input" class="form-control-label">Longitude</label></div>
                  <div class="col-9"><input type="double" id="number-input" name="tempat_longitude" placeholder="" class="gllpLongitude form-control" value="20"><small class="form-text text-muted"></small>
                  {!! $errors->first('tempat_longitude', '<p class="help-block">:message</p>') !!}
                  </div>
                </div>
              </div>
              {{-- update map --}}
              <div class="form-group">
                <div class="row">
                  <div class="col-3"></div>
                  <div class="col-9">
                    <input type="button" class="gllpUpdateButton btn btn-primary btn-visitor" value="Update Map">
                    <span class="info-update-map">Tombol update map digunakan apabila anda memasukkan logitude dan latitude secara langsung tanpa mengarahkan marker di peta</span>
                  </div>
                </div>
              </div>
              </fieldset>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-3"></div>
                <div class="col-9">
                  <hr class="hr-map">
                </div>
              </div>
            </div>
            {{-- Button submit --}}
            <div class="form-group">
              <div class="row">
                <div class="col-3"></div>
                <div class="col-9">
                  <button type="submit" class="btn btn-primary btn-visitor">
                    <i class="fa fa-dot-circle-o"></i> Submit
                  </button>
                  <button type="reset" class="btn btn-warning btn-visitor-muted"><a href="{{ url('/ulasan') }}">
                    <i class="fa fa-outline"></i> Kembali
                  </button>
                </div>
              </div>
            </div>  
            </form>
         </div>
        </div>
      </div>
    </section>

    <style>
      .subtitle-visitor {
        color: #fff;
      }
    </style>
@endsection