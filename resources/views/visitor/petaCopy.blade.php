@extends('layouts.visitor')
@section('contentFronfEnd')
    <!-- Contact -->
    <section id="service">
      <div class="container">
        <div class="row">
          <div class="col-3"></div>
          <div class="col-9 text-center">
            <h3 class="section-heading text-uppercase">PETA</h3>
            <h3 class="section-subheading text-muted"    style="margin-bottom: 30px">Rekomendasi tempat terdekat Anda !</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-3">
            <div class="card" style="border-top: 5px solid #303b7e;">
              <div class="card-body">
                <h4 class="title-rekomendasi">Filter Rekomendasi Tempat</h4>
                <div class="form-group">
                  <label class="label" for="address">Alamat</label>
                  <input id="address" class="form-control" type="textbox" value="" placeholder="Masukkan nama alamat">
                </div>
                <div class="form-group">
                  <label class="label" for="address">Kategori tempat</label>
                  <select class="form-control" id="category">
                    <option value="0">-Pilih Kategori-</option>
                    @foreach($kategori as $data)
                      <option value="{{ $data->id }}">{{ $data->kategori_nama }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label class="label" for="range">Radius</label>
                  <input type="range" min="0" max="10" value="5" id="radius" style="width: 100%; margin: 20px 0;" class="form-control">
                  <span style="font-size: 0.8rem; font-weight: 400; color: #fff;" class="btn btn-secondary btn-sm btn-block">
                    Radius pencarian: <span class="badge badge-light" id="radius_value"></span>
                  </span>
                  <hr>
                </div>
                <input id="submit" style="font-size: 1rem; font-weight: 400; color: #fff;" class="btn btn-success btn-block" type="button" value="Cari tempat">
              </div>
            </div>
          </div>
          <div class="col-lg-9">
              <div id="map"></div>
          </div>
        </div>
      </div>
    </section>
     <script>
      var slider = document.getElementById("radius");
      var output = document.getElementById("radius_value");
      
      output.innerHTML = slider.value;
      slider.oninput = function() {
        output.innerHTML = this.value;
      }

      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 10,
          center: {lat: -34.397, lng: 150.644}
        });
        var geocoder = new google.maps.Geocoder();

        document.getElementById('submit').addEventListener('click', function() {
          geocodeAddress(geocoder, map);
        });
      }

      function geocodeAddress(geocoder, resultsMap) {
        var address = document.getElementById('address').value;
        var category = document.getElementById('category');

        geocoder.geocode({'address': address}, function(results, status) {
          if (status === 'OK') {
            var location = results[0].geometry.location.lat() + "_" + results[0].geometry.location.lng();
            var radius = document.getElementById("radius").value;
            var category_value = category.options[category.selectedIndex].value;
            var url = '{{ url('haversine')}}';
            var new_url = url + '/' + location + '/' + radius + '/' + category_value
            window.location.href = new_url

          } else {
            alert('Hasil pencarian tidak ditemukan');
          }
        });
      }
    </script>
@endsection