@extends('layouts.visitor')
@section('contentFronfEnd')

    <!-- Services -->
    <section id="service">
      <div class="container">
        <div class="row">
          <div class="col-12 text-center">
            <h3 class="section-heading text-uppercase" style="margin-bottom: 20px;">Hasil Rekomendasi</h3>
            {{-- <h3 class="section-subheading text-muted">Hasil rekomendasi</h3> --}}
          </div>
          <div class="col-12">
            <div id="map" class="form-control" style="height: 300px; margin-bottom: 20px;"></div>
          </div>
        </div>
        <div class="row">
          @forelse($result as $data)
          <div class="col-4">
            <div class="card" style="margin-bottom: 20px;">
              <img src="{{asset('gambar_ulasan/'.$data->gambar_direktori)}}" alt="">
              <div class="card-body">
                <h5>{{$data->tempat_nama}}</h5>
                <small class="text-muted">Alamat</small>
                <p style="margin-bottom: 5px;">{{$data->tempat_alamat}}</p>
                <small class="text-muted">Nomor telepon</small>
                <p>{{$data->tempat_telepon}}</p>
                <a href="{{url('detail', $data->id)}}" class="btn btn-sm btn-secondary btn-block">Detail tempat</a>
              </div>
            </div>
          </div>
          @empty
          <div class="col-12">
            <div class="card">
              <div class="card-body text-center">
                <h3>Data yang anda cari tidak ditemukan</h3>
              </div>
            </div>
          </div>
          @endforelse
        </div>
      </div>
    </section>

   <script>
    function initMap() {
      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 9,
        center: {lat: -7.045669, lng: 110.679390}
      });
      var sites = {!! json_encode($result->toArray()) !!};
      var marker, i;
      for(i = 0; i < sites.length; i++) {
        // console.log(sites[i].tempat_latitude, sites[i].tempat_longitude)
        var marker = new google.maps.Marker({
            position: {lat: sites[i].tempat_latitude, lng: sites[i].tempat_longitude},
            title:"Hello World!",
            map: map
        });
      }
    }
  </script>

    {{-- <script type="text/javascript">
      data.forEach(value => {
        console.log('value',value.kategori_nama)
      })
    </script>

 --}}
@endsection