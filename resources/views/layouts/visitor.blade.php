<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>NearYou</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}">

    <!-- Custom fonts for this template -->
    {{-- <link rel="stylesheet" href="{{asset('vendor/font-awesome/css/font-awesome.min.css')}}"> --}}
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="{{asset('css/agency.min.css')}}">

    <link rel="stylesheet" href="{{asset('css/jquery-gmaps-latlon-picker.css')}}">
    <link rel="stylesheet" href="{{asset('css/demo.css')}}">

  </head>
    <style>
      html, body {
        margin: 0;
        padding: 0;
        height: 100%;
        width: 100%;
      }
      #marker {
        height: 400px;
        width: 100%;
      }
      #legend {
        font-family: Arial, sans-serif;
        background: #fff;
        padding: 10px;
        margin: 10px;
        border: 3px solid #000;
      }
      #legend h3 {
        margin-top: 0;
      }
      #legend img {
        vertical-align: middle;
      }
      .slidecontainer {
          width: 100%;
      }
      #coba {
    border:1px solid red;
    width: 800px;
    height: 500px;
}
    #map {
        height: 500px;
        width: 100%;
      }
    </style>

  <body id="page-top">

    <!-- Header -->
    <nav class="navbar navbar-expand-lg navbar-dark" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="{{ url('/')}}">NEAR YOU</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav text-uppercase ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ url('/peta')}}">Peta</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ url('/tambah_tempat')}}">Tambah Tempat Baru</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    @yield('contentFronfEnd')

  </body>

</html>


    <!-- Bootstrap core JavaScript -->

     {{-- <script src="{{asset('js/vendor/jquery-2.1.4.min.js')}}"></script> --}}
     {{-- <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script> --}}

    <!-- Plugin JavaScript -->
    {{-- <script src="{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script> --}}

    <!-- Contact form JavaScript -->
    {{-- <script src="{{asset('js/jqBootstrapValidation.js')}}"></script> --}}
    {{-- <script src="{{asset('js/contact_me.js')}}"></script> --}}

    <!-- Custom scripts for this template -->
    <script src="{{asset('js/agency.min.js')}}"></script>

    {{-- <script src="{{asset('js/jquery-2.1.1.min.js')}}"></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.24/gmaps.js"></script>
    <script src="{{asset('js/jquery-gmaps-latlon-picker.js')}}"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVYXCoe9UDADX4OM3rf2Wi5EQiTMp-Vxw&callback=initMap"></script>
    {{-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA_jUF9N0zsKeeZ-t9PtnmdhZlE0AInG_c&callback=initMap"></script> --}}
    {{-- <script src="{{asset('js/maps.google.polygon.containsLatLng.js')}}"></script> --}}

    {{-- ===================== FILTER ======================= --}}
    {{-- Geocode --}}
    
 
  {{-- <script>
var marker = new GMaps({
    el: '#marker',
    lat: -7.05855510764094, 
    lng: 110.42536254370793,
    zoom:12
  });

marker = marker.addMarker({
        lat:-7.05855510764094,
        lng:110.42536254370793,
    draggable: true,
});

  var searchBox = new google.maps.places.SearchBox(document.getElementById('searchmap'));

  google.maps.event.addListener(searchBox,'places_changed',function(){
        var places = searchBox.getPlaces();
        var bounds = new google.maps.LatLngBounds();
        var i, place;

        for(i=0; place=places[i];i++){
          bounds.extend(place.geometry.location);
          marker.setPosition(place.geometry.location);
        }
        map.fitBounds(bounds);
        map.setZoom(15);
    });

  google.maps.event.addListener(marker,'position_changed',function(){
    var lat = marker.getPosition().lat();
    var lng = marker.getPosition().lng();

    $('#lat').val(lat);
    $('#lng').val(lng);
  } ); 

var slider = document.getElementById("myRange");
var output = document.getElementById("demo");
output.innerHTML = slider.value;

slider.oninput = function() {
  output.innerHTML = this.value;
$('#rad').val(output.innerHTML);
};
   var coba = new GMaps({
      el: '#coba',
    lat: -7.05855510764094, 
    lng: 110.42536254370793,
    zoom:12
    });

  </script> --}}