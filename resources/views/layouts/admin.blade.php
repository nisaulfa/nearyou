<!doctype html>

<html class="no-js" lang=""> <!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Near You</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">

    <link rel="stylesheet" href="{{asset('css/normalize.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/cs-skin-elastic.css')}}">
    <link rel="stylesheet" href="{{asset('scss/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/jquery-gmaps-latlon-picker.css')}}">
    {{-- <link rel="stylesheet" href="{{asset('css/lib/vector-map/jqvmap.min.css')}}"> --}}

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #mape {
        height: 400px;
      }
    </style>

    {{-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> --}}

</head>
<body>


        <!-- Left Panel -->
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="./"><img src="images/logo.png" alt="Logo"></a>
                <a class="navbar-brand hidden" href="./"><img src="images/logo2.png" alt="Logo"></a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="{{ url('/home') }}"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                    </li>
                    <!-- /.Proses -->
                    <h3 class="menu-title">Proses</h3>
                    <li class="menu-item">
                        <a href="{{ url('/verifikasi_ulasan') }}"> <i class="menu-icon fa fa-check"></i>Verifikasi Ulasan Tempat</a>
                    </li>
                    <li class="menu-item">
                        <a href="{{ url('/verifikasi_laporan') }}"> <i class="menu-icon fa fa-check"></i>Verifikasi Laporan</a>
                    </li>
                    <!-- /.Tabel -->
                    <h3 class="menu-title">Tabel</h3><!-- /.menu-title -->
                    <li class="menu-item">
                        <a href="{{ url('/ulasan')}}"> <i class="menu-icon fa fa-table"></i>Tabel Ulasan Tempat</a>
                    </li>
                    <li class="menu-item">
                        <a href="{{ url('/laporan') }}"> <i class="menu-icon fa fa-warning"></i>Tabel Laporan</a>
                    </li>
                    <li class="menu-item">
                        <a href="{{ url('/kategori') }}"> <i class="menu-icon fa fa-delicious"></i>Tabel Kategori</a>
                    </li>

                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside>
    <!-- Left Panel -->

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">

        <!-- Header-->
        <header id="header" class="header">

            <div class="header-menu">

                <div class="col-sm-7">
                    <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                    <div class="header-left">

                    </div>
                </div>

                <div class="col-sm-5">
                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="user-avatar rounded-circle" src="images/admin.jpg" alt="User Avatar">
                        </a>
                        <a href="{{ url('/logout') }}"> logout </a>
                        <div class="user-menu dropdown-menu">
                                <a class="nav-link" href="{{url('logout')}}"><i class="fa fa-power -off"></i>Logout</a>
                        </div>
                    </div>

                </div>
            </div>

        </header><!-- /header -->
       @yield('content')
    </div><!-- /#right-panel -->

    <!-- Right Panel -->

    <script src="{{asset('js/vendor/jquery-2.1.4.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js')}}"></script>
    <script src="{{asset('js/plugins.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>


    <script src="{{asset('js/lib/chart-js/Chart.bundle.js')}}"></script>
    <script src="{{asset('js/dashboard.js')}}"></script>
    <script src="{{asset('js/widgets.js')}}"></script>

    <script src="{{asset('js/jquery-2.1.1.min.js')}}"></script>
    <script src="{{asset('js/jquery-gmaps-latlon-picker.js')}}"></script>
  
    {{-- <script src="{{asset('js/lib/vector-map/jquery.vmap.js')}}"></script> --}}
    {{-- <script src="{{asset('js/lib/vector-map/jquery.vmap.min.js')}}"></script> --}}
    {{-- <script src="{{asset('js/lib/vector-map/jquery.vmap.sampledata.js')}}"></script>
    <script src="{{asset('js/lib/vector-map/country/jquery.vmap.world.js')}}"></script> --}}
{{--     <script>
        ( function ( $ ) {
            "use strict";

            jQuery( '#vmap' ).vectorMap( {
                map: 'world_en',
                backgroundColor: null,
                color: '#ffffff',
                hoverOpacity: 0.7,
                selectedColor: '#1de9b6',
                enableZoom: true,
                showTooltip: true,
                values: sample_data,
                scaleColors: [ '#1de9b6', '#03a9f5' ],
                normalizeFunction: 'polynomial'
            } );
        } )( jQuery );
    </script> --}}
    <script>
        $(document).ready(function(){
            $("button").click(function(){
                $("p").toggle();
            });
        });
        </script>
     <script>
      var map;
      function initMap() {
        map = new google.maps.Map(document.getElementById('mape'), {
          center: {lat: -7.057810, lng: 110.429681},
          zoom: 8
        });
      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA_jUF9N0zsKeeZ-t9PtnmdhZlE0AInG_c&callback=initMap"></script>


</body>
</html>
