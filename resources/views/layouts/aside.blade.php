<!DOCTYPE html>
<html>
<head>
	<title>
		<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	   	<title>Sufee Admin - HTML5 Admin Template</title>
	    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
	    <meta name="viewport" content="width=device-width, initial-scale=1">

	    <link rel="apple-touch-icon" href="apple-icon.png">
	    <link rel="shortcut icon" href="favicon.ico">

	    <link rel="stylesheet" href="public/css/normalize.css">
	    <link rel="stylesheet" href="public/css/bootstrap.min.css">
	    <link rel="stylesheet" href="public/css/font-awesome.min.css">
	    <link rel="stylesheet" href="public/css/themify-icons.css">
	    <link rel="stylesheet" href="public/css/flag-icon.min.css">
	    <link rel="stylesheet" href="public/css/cs-skin-elastic.css">
	    <link rel="stylesheet" href="public/scss/style.css">
	    <link href="public/css/lib/vector-map/jqvmap.min.css" rel="stylesheet">

	    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
	</title>
</head>
<body>
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="./"><img src="images/logo.png" alt="Logo"></a>
                <a class="navbar-brand hidden" href="./"><img src="images/logo2.png" alt="Logo"></a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="index.html"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                    </li>
                    <!-- /.Tambah -->
                    <h3 class="menu-title">Tambah</h3>
                    <li class="menu-item">
                        <a href="#"> <i class="menu-icon fa fa-plus"></i>Tambah Ulasan Tempat</a>
                    </li>
                    <li class="menu-item">
                        <a href="#"> <i class="menu-icon fa fa-plus"></i>Tambah Kategori</a>
                    </li>
                    <!-- /.Proses -->
                    <h3 class="menu-title">Proses</h3>
                    <li class="menu-item">
                        <a href="#"> <i class="menu-icon fa fa-check"></i>Verifikasi Ulasan Tempat</a>
                    </li>
                    <li class="menu-item">
                        <a href="#"> <i class="menu-icon fa fa-check"></i>Verifikasi Laporan</a>
                    </li>
                    <li class="menu-item">
                        <a href="#"> <i class="menu-icon fa fa-map-marker"></i>Update Peta</a>
                    </li>
                    <!-- /.Tabel -->
                    <h3 class="menu-title">Tabel</h3><!-- /.menu-title -->
                    <li class="menu-item">
                        <a href="#"> <i class="menu-icon fa fa-table"></i>Tabel Ulasan Tempat</a>
                    </li>
                    <li class="menu-item">
                        <a href="#"> <i class="menu-icon fa fa-delicious"></i>Tabel Kategori</a>
                    </li>
                    <li class="menu-item">
                        <a href="#"> <i class="menu-icon fa fa-warning"></i>Tabel Laporan</a>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->

	<!-- /#skrip -->
	<script src="public/js/vendor/jquery-2.1.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
    <script src="public/js/plugins.js"></script>
    <script src="public/js/main.js"></script>


    <script src="public/js/lib/chart-js/Chart.bundle.js"></script>
    <script src="public/js/dashboard.js"></script>
    <script src="public/js/widgets.js"></script>
    <script src="public/js/lib/vector-map/jquery.vmap.js"></script>
    <script src="public/js/lib/vector-map/jquery.vmap.min.js"></script>
    <script src="public/js/lib/vector-map/jquery.vmap.sampledata.js"></script>
    <script src="public/js/lib/vector-map/country/jquery.vmap.world.js"></script>
    <script>
        ( function ( $ ) {
            "use strict";

            jQuery( '#vmap' ).vectorMap( {
                map: 'world_en',
                backgroundColor: null,
                color: '#ffffff',
                hoverOpacity: 0.7,
                selectedColor: '#1de9b6',
                enableZoom: true,
                showTooltip: true,
                values: sample_data,
                scaleColors: [ '#1de9b6', '#03a9f5' ],
                normalizeFunction: 'polynomial'
            } );
        } )( jQuery );
    </script>

</body>
</html>