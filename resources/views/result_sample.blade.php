@extends('layouts.visitor')
@section('contentFronfEnd')

    <nav class="navbar navbar-expand-lg navbar-dark bg-primary" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="{{url('/')}}">NEAR YOU</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
      </div>
    </nav>
    <!-- Services -->
    <section id="services">
      <div class="container">
        <div class="row">
          <div class="col-12 text-center">
            <h2 class="section-heading text-uppercase">RESULT SAMPLE</h2>
            <h3 class="section-subheading text-muted">Hasil rekomendasi</h3>
          </div>
          <div class="col-lg-2">
              @forelse($result as $data)
                <p>{{$data->tempat_nama}}</p>
                <p>{{$data->tempat_deskripsi}}</p>
                <p>{{$data->tempat_telepon}}</p>
                <p>{{$data->tempat_alamat}}</p>
                <p>{{$data->tempat_latitude}}</p>
                <p>{{$data->tempat_longitude}}</p>
                <a href="{{url('detail_sample', $data->id)}}" class="btn btn-sm btn-primary">Detail tempat</a>
                <hr>
              @empty
                <h3>Tidak ada data</h3>
              @endforelse
          </div>
          <div class="col-10">
            <div id="map"></div>
          </div>
        </div>
      </div>
    </section>
{{--     <?php 
    $api["tempat"] = $result; 
    echo json_encode($api["tempat"]); ?> --}}
   <script>
    function initMap() {
      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 9,
        center: {lat: -7.045669, lng: 110.679390}
      });
      var sites = {!! json_encode($result->toArray()) !!};
      var marker, i;
      for(i = 0; i < sites.length; i++) {
        // console.log(sites[i].tempat_latitude, sites[i].tempat_longitude)
        var marker = new google.maps.Marker({
            position: {lat: sites[i].tempat_latitude, lng: sites[i].tempat_longitude},
            title:"Hello World!",
            map: map
        });
      }
    }
  </script>

    {{-- <script type="text/javascript">
      data.forEach(value => {
        console.log('value',value.kategori_nama)
      })
    </script>

 --}}
@endsection