@extends('layouts.admin')
@section('content')

<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style>
.mySlides {display:none}
.w3-left, .w3-right, .w3-badge {cursor:pointer}
.w3-badge {height:13px;width:13px;padding:0}
.w3-text-white, .w3-hover-text-white:hover {
    color: black!important;
}
</style>
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Verifikasi Ulasan Tempat</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="{{ url('/home') }}">Dashboard</a></li>
                            <li><a href="{{ url('/verifikasi_ulasan') }}">Verifikasi Tempat</a></li>
                            <li class="active">Detail Ulasan Tempat</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

               <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title float-left">Detail Data</strong>
                            <div class="float-right">
                            <a class="btn btn-outline-success btn-sm" href="{{ url('/verifikasi_ulasan/verifikasi/' . $ulasan->id) }}" onclick="return confirm('Apakah Anda yakin memverifikasi data ?')" ><i class="fa fa-check"></i> Verifikasi</a>
                            <a class="btn btn-outline-danger btn-sm" href="{{ url('/verifikasi_ulasan/' . $ulasan->id . '/hapus') }}" onclick="return confirm('Apakah Anda yakin menghapus data ?')"><i class="fa fa-trash"></i> Hapus</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="w3-content w3-display-container" style="max-width:800px">
                                @foreach($data as $data)
                                    <img class="mySlides" style="padding: 0;margin: -10;" src="{{asset('gambar_ulasan/'.$data->gambar_direktori)}}" width="100%">
                                @endforeach

                              <div class="w3-center w3-container w3-section w3-large w3-text-white w3-display-bottommiddle" style="width:100%">
                                <div class="w3-left w3-hover-text-khaki" onclick="plusDivs(-1)">&#10094;</div>
                                <div class="w3-right w3-hover-text-khaki" onclick="plusDivs(1)">&#10095;</div>
                              </div>
                            </div>
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                          <table id="bootstrap-data-table" class="table table-hover">
                            @if($data->pengunjung_nama === 'Admin' && $data->pengunjung_email ==='Admin')
                            @else
                                <tr>
                                    <td>Pengirim</td>
                                    <td style="margin-right: -100px">:</td>
                                    <td>{{$data->pengunjung_nama}}</td>
                                </tr>
                                <tr>
                                    <td>Email Pengirim</td>
                                    <td style="margin-right: -100px">:</td>
                                    <td>{{$data->pengunjung_email}}</td>
                                </tr>
                            @endif
                            <tr>
                                <td>Tempat</td>
                                <td style="margin-right: -100px">:</td>
                                <td>{{$ulasan->tempat_nama}}</td>
                            </tr>
                            <tr>
                                <td>Telepon</td>
                                <td style="margin-right: -100px">:</td>
                                <td>{{$ulasan->tempat_telepon}}</td>
                            </tr>
                            <tr>
                                <td>Alamat</td>
                                <td style="margin-right: -100px">:</td>
                                <td>{{$ulasan->tempat_alamat}}</td>
                            </tr>
                            <tr>
                                <td>Kategori</td>
                                <td>:</td>
                                <td>{{$data->kategori_nama}}</td>
                            </tr>
                            <tr>
                                <td>Latitude</td>
                                <td>:</td>
                                <td>{{$ulasan->tempat_latitude}}</td>
                            </tr>
                            <tr>
                                <td>Longitude</td>
                                <td>:</td>
                                <td>{{$ulasan->tempat_longitude}}</td>
                            </tr>
                            <tr>
                                <td>Deskripsi</td>
                                <td>:</td>
                                <td>{{$ulasan->tempat_deskripsi}}</td>
                            </tr>
                            <tr>
                                <td>Status</td>
                                <td>:</td>
                                @if($ulasan->tempat_status ==='Terverifikasi')
                                    <td  ><h5 class="badge badge-info"> Terverifikasi </h5></td>
                                @else
                                    <td ><h5 class="badge badge-danger"> Belum Terverifikasi </h5></td>
                                @endif
                            </tr>
                        </table>
                        </div>
                        <div class="col-md-3"></div>
                        <div style="width: 100%; height: 300px;">
                                {!! Mapper::render() !!}
                            </div>
                        </div>
                        <div class="card-footer">
                         <a class="btn btn-outline-info btn-sm float-left" href="{{ url('/verifikasi_ulasan') }}"><i class="fa fa-backward"></i> Kembali</a> 
                        </div>
                    </div>
                </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
<script>
var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
     dots[i].className = dots[i].className.replace(" w3-white", "");
  }
  x[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " w3-white";
}
</script>
@endsection