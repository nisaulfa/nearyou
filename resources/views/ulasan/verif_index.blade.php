@extends('layouts.admin')
@section('content')

        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Verifikasi Ulasan Tempat</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Dashboard</a></li>
                            <li class="active">Verifikasi Tempat</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Tabel Verifikasi Ulasan Tempat</strong>
                        </div>
                        <div class="card-body">
                          <table id="bootstrap-data-table" class="table table-striped table-bordered">
                            <thead style="font-size: 15px">
                              <tr>
                                <th>No</th>
                                <th>Waktu Masuk</th>
                                <th>Aksi</th>
                                <th>Tempat</th>
                                <th>Kategori</th>
                              </tr>
                            </thead>
                                    <tbody style="font-size: 13px">
                                    @foreach($ulasan as $index=>$ulasan)
                                      <tr>
                                        <td>{{$index+1}}</td>
                                        <td>`</td>
                                        <td>
                                            <a class="btn btn-outline-primary btn-sm" href="{{ url('/verifikasi_ulasan/' . $ulasan->id) }}" style="font-size: 12px"><i class="fa fa-list"></i> Detail</a> |
                                            <a class="btn btn-outline-success btn-sm" href="{{ url('/verifikasi_ulasan/verifikasi/' . $ulasan->id) }}" onclick="return confirm('Apakah Anda yakin memverifikasi data ?')" style="font-size: 12px"><i class="fa fa-check"></i> Verifikasi</a> |
                                            <a class="btn btn-outline-danger btn-sm" href="{{ url('/verifikasi_ulasan/' . $ulasan->id . '/hapus') }}" onclick="return confirm('Apakah Anda yakin menghapus data ?')" style="font-size: 12px"><i class="fa fa-trash"></i> Hapus</a>
                                        </td>
                                        <td>{{$ulasan->tempat_nama}}</td>
                                        <td>{{$ulasan->kategori_nama}}</td>

                                      </tr>
                                    @endforeach
                                    </tbody>
                          </table>
                        </div>
                    </div>
                </div>


                </div>
            </div><!-- .animated -->
        </div><!-- .content -->

    <!-- .data tabel -->

    <script src="{{asset('js/lib/data-table/datatables.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/jszip.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/pdfmake.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/vfs_fonts.js')}}"></script>
    <script src="{{asset('js/lib/data-table/buttons.html5.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/buttons.print.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/datatables-init.js')}}"></script>


    <script type="text/javascript">
        $(document).ready(function() {
          $('#bootstrap-data-table-export').DataTable();
        } );
    </script>

@endsection