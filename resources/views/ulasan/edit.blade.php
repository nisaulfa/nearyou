@extends('layouts.admin')
@section('content')

        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Ulasan Tempat</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="{{ url('/home') }}">Dashboard</a></li>
                            <li><a href="{{ url('/ulasan') }}">Ulasan Tempat</a></li>
                            <li class="active">Ubah Ulasan Tempat</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">               
                  <div class="col-lg-12">
                    <div class="card">
                      <div class="card-header">
                        <strong>Ubah Ulasan Tempat</strong>
                      </div>
                      <div class="card-body card-block">
                        <form action="{{url()->current()}}" method="post" class="form-horizontal" enctype="multipart/form-data">
                          {{csrf_field()}}
                      </br>
                        <div class="col-lg-6">
                        <!-- nama pengirim -->
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Nama Pengirim</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="" disabled="" placeholder="Text" class="form-control" value="{{$ulasan->pengunjung_nama}}"></div>
                          </div>
                        <!-- nama tempat -->
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Nama Tempat</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="tempat_nama" placeholder="Text" class="form-control" value="{{$ulasan->tempat_nama}}"></div>
                          </div>
                        <!-- deskripsi -->
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Deskripsi Tempat</label></div>
                            <div class="col-12 col-md-9"><textarea name="tempat_deskripsi" id="textarea-input" rows="9" placeholder="Deskripsi" class="form-control" >{{$ulas->tempat_deskripsi}}</textarea></div>
                          </div>
                        <!-- Kategori -->
                          <div  class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Kategori</label></div>
                            <div class="col-12 col-md-9">
                              <select class="form-control" name="kategori_id">
                                  <option value="{{ $ulas->kategori_id }}">{{ $ulas->kategori_nama }}</option>
                                  @foreach($kategori as $data)
                                  <option value="{{ $data->id }}">{{ $data->kategori_nama }}</option>
                                  @endforeach
                              </select>
                            </div>
                          </div>
                        <!-- Input Image -->  
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="file-input" class=" form-control-label">Masukkan Foto Tempat</label></div>
                            <div class="col-12 col-md-9">
                              <span class="input-group-btn">
                                <!-- image-preview-clear button -->
                                <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                  <span class="glyphicon glyphicon-remove"></span> Clear
                                </button>
                                <!-- image-preview-input -->
                                <div class="btn btn-default image-preview-input">
                                  <span class="glyphicon glyphicon-folder-open"></span>
                                  <span class="image-preview-input-title"></span>
                                  <input type="file" accept="image/png, image/jpeg, image/jpg" name="gambar_direktori"/> <!-- rename it -->
                                </div>
                              </span>
                            </div>
                          </div>
                        <!-- Status -->
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="select" class=" form-control-label">Status</label></div>
                            <div class="col-12 col-md-9">
                              <select name="tempat_status" id="select" class="form-control" >
                                <option >{{$ulas->tempat_status}}</option>
                                <option value="Belum Terverifikasi">Belum Terverifikasi</option>
                                <option value="Terverifikasi">Terverifikasi</option>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-6">
                          <fieldset class="gllpLatlonPicker">
                        <!-- email pengirim -->
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Email Pengirim</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="" disabled="" placeholder="Text" class="form-control" value="{{$ulasan->pengunjung_email}}"></div>
                          </div>
                        <!-- nomor telepon -->
                          <div class="row form-group" {{ $errors->has('tempat_telepon') ? 'has-error' : '' }}>
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Nomor Telepon</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="tempat_telepon" placeholder="Text" class="form-control" value="{{$ulas->tempat_telepon}}"></div>
                            {!! $errors->first('tempat_telepon', '<p class="help-block">:message</p>') !!}
                          </div>
                        <!-- Alamat -->
                          <div class="row form-group" {{ $errors->has('tempat_alamat') ? 'has-error' : '' }}>
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Alamat</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="tempat_alamat" placeholder="Text" class="form-control" value="{{$ulas->tempat_alamat}}"></div>
                            {!! $errors->first('tempat_alamat', '<p class="help-block">:message</p>') !!}
                          </div>
                            <div class="gllpMap">Google Maps</div>
                            <br/>
                          <!-- Latitude -->
                              <div  class="row form-group" {{ $errors->has('tempat_latitude') ? 'has-error' : '' }}>
                                  <div class="col col-md-3"><label for="text-input" class=" form-control-label">Latitude</label></div>
                                  <div class="col-12 col-md-9"><input type=
                                    "double" id="number-input" name="tempat_latitude" placeholder="" class="gllpLatitude" value="{{$ulas->tempat_latitude}}" ><small class="form-text text-muted"></small></div>
                                  {!! $errors->first('tempat_latitude', '<p class="help-block">:message</p>') !!}
                              </div>
                          <!-- longitude -->
                              <div class="row form-group" {{ $errors->has('tempat_longitude') ? 'has-error' : '' }}>
                                <div class="col col-md-3"><label for="text-input" class=" form-control-label">Longitude</label></div>
                                <div class="col-12 col-md-9"><input type="double" id="number-input" name="tempat_longitude" placeholder="" class="gllpLongitude" value="{{$ulas->tempat_longitude}}"><small class="form-text text-muted"></small></div>
                                {!! $errors->first('tempat_longitude', '<p class="help-block">:message</p>') !!}
                              </div>
                            <input type="button" class="gllpUpdateButton" value="update map">
                            <br/>
                          </fieldset>
                      </div> {{-- 6col --}} 
                    </div>
                       <div class="card-footer">
                            <button type="submit" class="btn btn-primary btn-sm">
                              <i class="fa fa-dot-circle-o"></i> Submit
                            </button>
                            <button type="reset" class="btn btn-danger btn-sm">
                              <i class="fa fa-ban"></i> Reset
                            </button>
                      </div>

                        </form>
                      </div>
                   </div><!-- .card -->

                </div><!-- .col -->

                </div><!-- .row -->
            </div><!-- .animated -->
        </div><!-- .content -->

@endsection