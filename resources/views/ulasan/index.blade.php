@extends('layouts.admin')
@section('content')

        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Ulasan Tempat</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                        <a class="btn btn-primary btn-sm" href="{{ url('/tambah_ulasan') }}" style="margin-top: 10px" ><i class="fa fa-plus"></i> Tambah Ulasan Tempat</a>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Tabel Ulasan Tempat</strong>
                        </div>
                        <div class="card-body">
                          <table id="bootstrap-data-table" class="table table-striped table-bordered">
                            <thead style="font-size: 15px">
                              <tr>
                                <th>No</th>
                                <th>Tempat</th>
                                <th>Kategori</th>
                                <th>Status</th>
                                <th>Aksi</th>
                              </tr>
                            </thead>
                            <tbody >
                            @foreach($ulasan as $index=>$ulasan)
                              <tr>
                                <td>{{$index+1}}</td>
                                <td>{{$ulasan->tempat_nama}}</td>
                                <td>{{$ulasan->kategori_nama}}</td>
                                @if($ulasan->tempat_status ==='Terverifikasi')
                                    <td class="text-center" ><h5 style="width: 75%" class="badge badge-info"> Terverifikasi </h5></td>
                                @else
                                    <td class="text-center" ><h5 style="width: 75%" class="badge badge-danger"> Belum Terverifikasi </h5></td>
                                @endif
                                <td>
                                	<a class="btn btn-outline-primary btn-sm" href="{{ url('/ulasan/' . $ulasan->id) }}" style="font-size: 12px"><i class="fa fa-list"></i> Detail</a> |
                                	<a class="btn btn-outline-warning btn-sm" href="{{ url('/ulasan/' . $ulasan->id . '/edit') }}" style="font-size: 12px"><i class="fa fa-pencil"></i> Ubah</a> |
                                    <a class="btn btn-outline-danger btn-sm" href="{{ url('/ulasan/' . $ulasan->id . '/hapus') }}" onclick="return confirm('Apakah Anda yakin menghapus data ?')" style="font-size: 12px"><i class="fa fa-trash"></i> Hapus</a>
                                </td>
                              </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </div>
                    </div>
                </div>


                </div>
            </div><!-- .animated -->
        </div><!-- .content -->

    <!-- .data tabel -->

    <script src="{{asset('js/lib/data-table/datatables.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/jszip.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/pdfmake.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/vfs_fonts.js')}}"></script>
    <script src="{{asset('js/lib/data-table/buttons.html5.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/buttons.print.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/datatables-init.js')}}"></script>


    <script type="text/javascript">
        $(document).ready(function() {
          $('#bootstrap-data-table-export').DataTable();
        } );
    </script>

@endsection