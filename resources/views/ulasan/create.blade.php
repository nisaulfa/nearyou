@extends('layouts.admin')
@section('content')
{{-- 
    <link rel="stylesheet" href="{{asset('css/demo.css')}}"> --}}
      <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Ulasan Tempat</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="{{ url('/home') }}">Dashboard</a></li>
                            <li><a href="{{ url('/ulasan') }}">Ulasan Tempat</a></li>
                            <li class="active">Tambah Ulasan Tempat</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row" style="margin-bottom: : 15px">
                    <div class="card">
                      <div class="card-header">
                        <strong>Masukkan Ulasan Tempat Baru</strong>
                      </div>
                      <div class="card-body card-block">
                        <form action="{{url()->current()}}" method="post" class="form-horizontal" enctype="multipart/form-data">
                          {{csrf_field()}}
                         <div class="col-lg-6">
                        <!-- nama tempat -->
                      </br>
                          <div class="row form-group" {{ $errors->has('tempat_nama') ? 'has-error' : '' }}>
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Nama Tempat</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="tempat_nama" placeholder="Text" class="form-control"><small class="form-text text-muted">ex. RM Padang Murah</small></div>
                            {!! $errors->first('tempat_nama', '<p class="help-block">:message</p>') !!}
                          </div>
                        <!-- deskripsi -->
                          <div class="row form-group" {{ $errors->has('tempat_deskripsi') ? 'has-error' : '' }}>
                            <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Deskripsi Tempat</label></div>
                            <div class="col-12 col-md-9"><textarea name="tempat_deskripsi" id="textarea-input" rows="9" placeholder="Deskripsi" class="form-control"></textarea></div>
                          {!! $errors->first('tempat_deskripsi', '<p class="help-block">:message</p>') !!}
                          </div>
                        <!-- Kategori -->
                          <div  class="row form-group" {{ $errors->has('kategori_id') ? 'has-error' : '' }}>
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Kategori</label></div>
                            <div class="col-12 col-md-9">
                              <select class="form-control" name="kategori_id">
                                  <option value="">-Pilih Kategori-</option>
                                  @foreach($kategori as $data)
                                  <option value="{{ $data->id }}">{{ $data->kategori_nama }}</option>
                                  @endforeach
                              </select>
                            </div>
                            {!! $errors->first('kategori_id', '<p class="help-block">:message</p>') !!}
                        </div>
                        <!-- Input Image -->  
                          <div class="row form-group" {{ $errors->has('gambar_direktori') ? 'has-error' : '' }}>
                            <div class="col col-md-3"><label for="file-input" class=" form-control-label">Masukkan Foto Tempat</label></div>
                            <div class="col-12 col-md-9">
                              <span class="input-group-btn">
                                <!-- image-preview-clear button -->
                                <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                  <span class="glyphicon glyphicon-remove"></span> Clear
                                </button>
                                <!-- image-preview-input -->
                                <div class="btn btn-default image-preview-input">
                                  <span class="glyphicon glyphicon-folder-open"></span>
                                  <span class="image-preview-input-title"></span>
                                  <input type="file" accept="image/png, image/jpeg, image/gif" name="gambar_direktori[]"/ multiple=""> <!-- rename it -->
                                </div>
                              </span>
                            </div>
                            {!! $errors->first('gambar_direktori', '<p class="help-block">:message</p>') !!}
                          </div>
                        <!-- Status -->
                          <div class="row form-group" {{ $errors->has('tempat_status') ? 'has-error' : '' }}>
                            <div class="col col-md-3"><label for="select" class=" form-control-label">Status</label></div>
                            <div class="col-12 col-md-9">
                              <select name="tempat_status" id="select" class="form-control">
                                <option value="">Status Verifikasi</option>
                                <option value="Belum Terverifikasi">Belum Terverifikasi</option>
                                <option value="Terverifikasi">Terverifikasi</option>
                              </select>
                            </div>
                            {!! $errors->first('tempat_status', '<p class="help-block">:message</p>') !!}
                          </div>
                        </div>
                        <div class="col-lg-6" style="margin-top: 23px" >
                        <fieldset class="gllpLatlonPicker">
                        <!-- nomor telepon -->
                          <div class="row form-group" {{ $errors->has('tempat_telepon') ? 'has-error' : '' }}>
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Nomor Telepon</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="tempat_telepon" placeholder="Text" class="form-control"></div>
                            {!! $errors->first('tempat_telepon', '<p class="help-block">:message</p>') !!}
                          </div>
                        <!-- Alamat -->
                          <div class="row form-group" {{ $errors->has('tempat_alamat') ? 'has-error' : '' }}>
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Alamat</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="tempat_alamat" placeholder="Text" class="form-control"></div>
                            {!! $errors->first('tempat_alamat', '<p class="help-block">:message</p>') !!}
                          </div>
                            <div class="gllpMap">Google Maps</div>
                            <br/>
                          <!-- Latitude -->
                              <div  class="row form-group" {{ $errors->has('tempat_latitude') ? 'has-error' : '' }}>
                                  <div class="col col-md-3"><label for="text-input" class=" form-control-label">Latitude</label></div>
                                  <div class="col-12 col-md-9"><input type=
                                    "double" id="number-input" name="tempat_latitude" placeholder="" class="gllpLatitude" value="20" ><small class="form-text text-muted"></small></div>
                                  {!! $errors->first('tempat_latitude', '<p class="help-block">:message</p>') !!}
                              </div>
                          <!-- longitude -->
                              <div class="row form-group" {{ $errors->has('tempat_longitude') ? 'has-error' : '' }}>
                                <div class="col col-md-3"><label for="text-input" class=" form-control-label">Longitude</label></div>
                                <div class="col-12 col-md-9"><input type="double" id="number-input" name="tempat_longitude" placeholder="" class="gllpLongitude" value="20"><small class="form-text text-muted"></small></div>
                                {!! $errors->first('tempat_longitude', '<p class="help-block">:message</p>') !!}
                              </div>
                            <input type="button" class="gllpUpdateButton" value="update map">
                            <br/>
                          </fieldset>
                        </div>
                          <div class="card-footer">
                            <button type="submit" class="btn btn-primary btn-sm">
                              <i class="fa fa-dot-circle-o"></i> Submit
                            </button>
                            <button type="reset" class="btn btn-warning btn-sm"><a href="{{ url('/ulasan') }}">
                              <i class="fa fa-outline"></i> Kembali
                            </button>
                        </div>
                        </form>
                      </div>
                        
                   </div><!-- .card -->

                </div><!-- .row -->
            </div><!-- .animated -->
        </div><!-- .content -->


@endsection