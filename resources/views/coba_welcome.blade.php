<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- ============================================= -->
    
    <!-- ============================================= -->
    <!-- ============================================= -->
    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">
{{--     <link rel="stylesheet" href="{{asset('css/normalize.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/cs-skin-elastic.css')}}">
    <link rel="stylesheet" href="{{asset('scss/style.css')}}"> --}}
    <!-- =============================================== -->

    <title>Near You</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}">

    <!-- Custom fonts for this template -->
    <link rel="stylesheet" href="{{asset('vendor/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Kaushan+Script">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700">
    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="{{asset('css/agency.min.css')}}">

{{-- ==================PETA============================== --}}
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <link rel="stylesheet" href="{{asset('css/demo.css')}}">

  <!-- Dependencies: JQuery and GMaps API should be loaded first -->
  <script src="{{asset('js/jquery-2.1.1.min.js')}}"></script>
  <script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
{{-- <script src="http://maps.googleapis.com/api/js?key=AIzaSyA_jUF9N0zsKeeZ-t9PtnmdhZlE0AInG_c"></script> --}}
  <!-- CSS and JS for our code -->
  <link rel="stylesheet" href="{{asset('css/jquery-gmaps-latlon-picker.css')}}">
  <script src="{{asset('js/jquery-gmaps-latlon-picker.js')}}"></script>

  <style>
    #input-img > input {
      display: none;
    }
  </style>
  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">NEAR YOU</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav text-uppercase ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#services">Peta</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">Tambah Tempat Baru</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Header -->
    <header class="masthead">
      <div class="container">
        <div class="intro-text">
          <div class="intro-lead-in">Aplikasi rekomendasi tempat terdekat Anda!</div>
          <div class="intro-heading text-uppercase">Near You</div>
          <a class="btn btn-info btn-xl text-uppercase js-scroll-trigger" href="#services">Peta</a>
          <a class="btn btn-secondary btn-xl text-uppercase js-scroll-trigger" href="#contact">Tambah Tempat Baru</a>
        </div>
      </div>
    </header>

    <!-- Services -->
    <section id="services">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Peta</h2>
            <h3 class="section-subheading text-muted">Rekomendasi Tempat terdekar Anda dalam jarak 5km</h3>
          </div>
        </div>
      </div>
    </section>


    <!-- Contact -->
    <section id="contact">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Rekomendasi Tempat Baru</h2>
            <h3 class="section-subheading text-muted"    style="margin-bottom: 30px">Kontribusi Anda sangat berarti bagi kami</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
                        <form action="{{route('simpan_rekomendasi')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
                          {{csrf_field()}}
                         <div class="col-lg-6">
                        <!-- nama tempat -->
                          <div class="row form-group" {{ $errors->has('tempat_nama') ? 'has-error' : '' }}>
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Nama Tempat</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="tempat_nama" placeholder="Text" class="form-control"><small class="form-text text-muted">ex. RM Padang Murah</small></div>
                            {!! $errors->first('tempat_nama', '<p class="help-block">:message</p>') !!}
                          </div>
                        <!-- deskripsi -->
                          <div class="row form-group" {{ $errors->has('tempat_deskripsi') ? 'has-error' : '' }}>
                            <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Deskripsi Tempat</label></div>
                            <div class="col-12 col-md-9"><textarea name="tempat_deskripsi" id="textarea-input" rows="9" placeholder="Deskripsi" class="form-control"></textarea></div>
                          {!! $errors->first('tempat_deskripsi', '<p class="help-block">:message</p>') !!}
                          </div>
                        <!-- Kategori -->
                          <div  class="row form-group" {{ $errors->has('kategori_id') ? 'has-error' : '' }}>
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Kategori</label></div>
                            <div class="col-12 col-md-9">
                              <select class="form-control" name="kategori_id">
                                  <option value="">-Pilih Kategori-</option>
                                  @foreach($kategori as $data)
                                  <option value="{{ $data->id }}">{{ $data->kategori_nama }}</option>
                                  @endforeach
                              </select>
                            </div>
                            {!! $errors->first('kategori_id', '<p class="help-block">:message</p>') !!}
                        </div>
                        <!-- Input Image -->  
                          <div class="row form-group" {{ $errors->has('gambar_direktori') ? 'has-error' : '' }}>
                            <div class="col col-md-3"><label for="file-input" class=" form-control-label">Masukkan Foto Tempat</label></div>
                            <div class="col-12 col-md-9">
                              <span class="input-group-btn">
                                <!-- image-preview-clear button -->
                                <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                  <span class="glyphicon glyphicon-remove"></span> Clear
                                </button>
                                <!-- image-preview-input -->
                                <div class="btn btn-default image-preview-input">
                                  <span class="glyphicon glyphicon-folder-open"></span>
                                  <span class="image-preview-input-title"></span>
                                  <input type="file" accept="image/png, image/jpeg, image/gif" name="gambar_direktori[]"/ multiple=""> <!-- rename it -->
                                </div>
                              </span>
                            </div>
                            {!! $errors->first('gambar_direktori', '<p class="help-block">:message</p>') !!}
                          </div>
                        <!-- Status -->
                          <div class="row form-group" {{ $errors->has('tempat_status') ? 'has-error' : '' }}>
                            {{-- <div class="col col-md-3"><label for="select" class=" form-control-label">Status</label></div> --}}
                            {{-- <div class="col-12 col-md-9">
                              <select name="tempat_status" id="select" class="form-control">
                                <option value="">Status Verifikasi</option>
                                <option value="Belum Terverifikasi">Belum Terverifikasi</option>
                                <option value="Terverifikasi">Terverifikasi</option>
                              </select>
                            </div> --}}
                            {!! $errors->first('tempat_status', '<p class="help-block">:message</p>') !!}
                          </div>
                        </div>
                        <div class="col-lg-6">
                          <fieldset class="gllpLatlonPicker">
                            <br/><br/>
                            <div class="gllpMap">Google Maps</div>
                            <br/>
                              <!-- Latitude -->
                              <div  class="row form-group" {{ $errors->has('tempat_latitude') ? 'has-error' : '' }}>
                                  <div class="col col-md-3"><label for="text-input" class=" form-control-label">Latitude</label></div>
                                  <div class="col-12 col-md-9"><input type=
                                    "double" id="number-input" name="tempat_latitude" placeholder="" class="gllpLatitude" value="20" ><small class="form-text text-muted"></small></div>
                                  {!! $errors->first('tempat_latitude', '<p class="help-block">:message</p>') !!}
                              </div>
                              <!-- longitude -->
                              <div class="row form-group" {{ $errors->has('tempat_longitude') ? 'has-error' : '' }}>
                                <div class="col col-md-3"><label for="text-input" class=" form-control-label">Longitude</label></div>
                                <div class="col-12 col-md-9"><input type="double" id="number-input" name="tempat_longitude" placeholder="" class="gllpLongitude" value="20"><small class="form-text text-muted"></small></div>
                                {!! $errors->first('tempat_longitude', '<p class="help-block">:message</p>') !!}
                              </div>
                              {{-- zoom --}}
                              <div class="row form-group" >
                                <div class="col col-md-3"><label class=" form-control-label">Zoom</label></div>
                                <div class="col-12 col-md-9"><input type="number" id="number-input" name="zoom" placeholder="" class="gllpZoom" value="3"><small class="form-text text-muted"></small></div>
                              </div>
                            <input type="button" class="gllpUpdateButton" value="update map">
                            <br/>
                          </fieldset>
                        </div>
                          <div class="card-footer">
                            <button type="submit" class="btn btn-primary btn-sm">
                              <i class="fa fa-dot-circle-o"></i> Submit
                            </button>
                            <button type="reset" class="btn btn-warning btn-sm"><a href="{{ url('/ulasan') }}">
                              <i class="fa fa-outline"></i> Kembali
                            </button>
                        </div>
                        </form>
                       </div>
        </div>
      </div>
    </section>

    <!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <span class="copyright">Copyright &copy; Your Website 2018</span>
          </div>
          <div class="col-md-4">
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-linkedin"></i>
                </a>
              </li>
            </ul>
          </div>
          <div class="col-md-4">
            <ul class="list-inline quicklinks">
              <li class="list-inline-item">
                <a href="#">Privacy Policy</a>
              </li>
              <li class="list-inline-item">
                <a href="#">Terms of Use</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    
    <!-- Plugin JavaScript -->
    <script src="{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>

    <!-- Contact form JavaScript -->
    <script src="{{asset('js/jqBootstrapValidation.js')}}"></script>
    <script src="{{asset('js/contact_me.js')}}"></script>

    <!-- Custom scripts for this template -->
    <script src="{{asset('js/agency.min.js')}}"></script>

    <!-- ========================================= -->
    <script src="{{asset('js/vendor/jquery-2.1.4.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js')}}"></script>
    <script src="{{asset('js/plugins.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('js/lib/chart-js/Chart.bundle.js')}}"></script>
    <script src="{{asset('js/dashboard.js')}}"></script>
    <script src="{{asset('js/widgets.js')}}"></script>
    <script src="{{asset('js/lib/vector-map/jquery.vmap.js')}}"></script>
    <script src="{{asset('js/lib/vector-map/jquery.vmap.min.js')}}"></script>
    <script src="{{asset('js/lib/vector-map/jquery.vmap.sampledata.js')}}"></script>
    <script src="{{asset('js/lib/vector-map/country/jquery.vmap.world.js')}}"></script>
    <!-- ========================================= -->

  </body>
</html>
