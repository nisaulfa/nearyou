@extends('layouts.visitor')
@section('contentFronfEnd')

    <header class="masthead">
      <div class="container">
        <div class="intro-text">
          <div class="intro-lead-in">Aplikasi rekomendasi tempat terdekat Anda!</div>
          <div class="intro-heading text-uppercase">Near You</div>
          <a class="btn btn-info btn-xl text-uppercase js-scroll-trigger" href="{{ url('/peta')}}">Peta</a>
          <a class="btn btn-secondary btn-xl text-uppercase js-scroll-trigger" href="{{ url('/tambah_tempat')}}">Tambah Tempat Baru</a>
        </div>
      </div>
    </header>


@endsection