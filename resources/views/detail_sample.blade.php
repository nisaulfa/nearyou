@extends('layouts.visitor')
@section('contentFronfEnd')

    <nav class="navbar navbar-expand-lg navbar-dark bg-primary" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="{{url('/')}}">NEAR YOU</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
      </div>
    </nav>
    <!-- Services -->
    <section id="services">
      <div class="container">
        <div class="row">
          <div class="col-12 text-center">
            <h2 class="section-heading text-uppercase">DETAIL ULASAN {{$data->tempat_nama}}</h2>
          </div>
          <div class="col-3">
            <h3>{{$data->tempat_nama}}</h3>
            <p>{{$data->tempat_deskripsi}}</p>
            <p>{{$data->tempat_telepon}}</p>
            <p>{{$data->tempat_alamat}}</p>
            <p>{{$data->tempat_latitude}}</p>
            <p>{{$data->tempat_longitude}}</p>

            <a href="{{url('review_tempat', $data->id)}}" class="btn btn-sm btn-info">Review Tempat</a>
            <a href="{{url('lapor_tempat', $data->id)}}" class="btn btn-sm btn-danger">Laporkan Tempat</a>
            <hr>
          </div>
          <div class="col-9">
            <div id="map" style="height: 300px !important;"></div>
          </div>
        </div>
        <div class="row"> 
            <div class="col-12">  
              <hr> 
                <h3>Review ulasan tempat</h3>
            </div>
        </div>
      </div>
    </section>


   <script>
    function initMap() {
      var sites = {!!json_encode($data->toArray())!!};
      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 17,
        center: {lat: sites.tempat_latitude, lng: sites.tempat_longitude}
      });
      var marker = new google.maps.Marker({
          position: {lat: sites.tempat_latitude, lng: sites.tempat_longitude},
          title:"Hello World!",
          map: map
      });
    }
  </script>

@endsection