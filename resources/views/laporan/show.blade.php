@extends('layouts.admin')
@section('content')
      <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Laporan Tempat</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="{{ url('/home') }}">Dashboard</a></li>
                            <li><a href="{{ url('/laporan') }}">Laporan Tempat</a></li>
                            <li class="active">Detail Laporan Tempat</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

               <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title float-left">Detail Laporan</strong>
                        </div>
                        <div class="card-body">
                          <div class="col-md-3"></div>
                          <div class="col-md-6">
                          <table id="bootstrap-data-table" class="table table-hover">
                            <tr>
                                <td>Nama Pelapor</td>
                                <td style="margin-right: -100px">:</td>
                                <td>{{$laporan->pengunjung_nama}}</td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td style="margin-right: -100px">:</td>
                                <td>{{$laporan->pengunjung_email}}</td>
                            </tr>
                            <tr>
                                <td>Tempat</td>
                                <td style="margin-right: -100px">:</td>
                                <td>{{$laporan->tempat_nama}}</td>
                            <tr>
                                <td>Deskripsi</td>
                                <td>:</td>
                                <td>{{$laporan->laporan_deskripsi}}</td>
                            </tr>
                            <tr>
                                <td>Status</td>
                                <td>:</td>
                                @if($laporan->laporan_status ==='Tampil')
                                    <td  ><h5 class="badge badge-success"> Tampil </h5></td>
                                @else
                                    <td " ><h5 class="badge badge-danger"> Tidak Tampil </h5></td>
                                @endif
                            </tr>
                        </table>
                        </div>
                        <div class="col-md-3"></div>
                        </div>
                        <div class="card-footer">
                            <button type="back" class="btn btn-info btn-sm float-left"><a href="{{ url('/laporan')}}">
                              <i class="fa fa-backward"></i> Kembali
                            </button>
                            <button class="btn btn-warning btn-sm float-right"><a href="{{ url('/ulasan/' . $laporan->tempat_id) }}"">
                              <i class="fa fa-forward"></i> Ulasan Tempat yang dilaporkan
                            </button>
                        </div>
                    </div>
                </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->

@endsection