@extends('layouts.admin')
@section('content')

        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Verifikasi Laporan</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="{{ url('/home') }}">Dashboard</a></li>
                            <li><a href="{{ url('/verifikasi_laporan') }}">Verifikasi Laporan</a></li>
                            <li class="active">Laporan Ulasan Tempat</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

               <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title float-left">Detail Data</strong>
                            <div class="float-right">
                            <a class="btn btn-outline-success btn-sm" href="{{ url('/verifikasi_laporan/verifikasi/' . $laporan->id) }}" onclick="return confirm('Apakah Anda yakin memverifikasi data ?')" ><i class="fa fa-check"></i> Verifikasi</a>
                            <a class="btn btn-outline-danger btn-sm" href="{{ url('/verifikasi_laporan/' . $laporan->id . '/hapus') }}" onclick="return confirm('Apakah Anda yakin menghapus data ?')"><i class="fa fa-trash"></i> Hapus</a>
                            </div>
                        </div>
                        <div class="card-body col-md-12">
                        <div class="col-md-1"></div>
                        <div class="col-sm-10">
                          <table id="bootstrap-data-table" class="table table-hover">
                            <tr>
                                <td>Nama Pelapor</td>
                                <td style="margin-right: -100px">:</td>
                                <td>{{$laporan->pengunjung_nama}}</td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td style="margin-right: -100px">:</td>
                                <td>{{$laporan->pengunjung_email}}</td>
                            </tr>
                            <tr>
                                <td>Tempat</td>
                                <td style="margin-right: -100px">:</td>
                                <td>{{$laporan->tempat_nama}}</td>
                            </tr>
                            <tr>
                                <td>Deskripsi</td>
                                <td>:</td>
                                <td>{{$laporan->laporan_deskripsi}}</td>
                            </tr>
                            <tr>
                                <td>Status</td>
                                <td>:</td>
                                @if($laporan->laporan_status ==='Terverifikasi')
                                    <td  ><h5 class="badge badge-success">Terverifikasi </h5></td>
                                @else
                                    <td " ><h5 class="badge badge-danger">Belum Terverifikasi</h5></td>
                                @endif
                            </tr>
                        </table>
                        </div>
                        <div class="card-footer">
                         <a class="btn btn-outline-info btn-sm" href="{{ url('/verifikasi_laporan') }}"><i class="fa fa-backward"></i> Kembali</a>
                         <a class="btn btn-outline-warning btn-sm float-right" href="{{ url('/ulasan/' . $laporan->tempat_id) }}"">
                              <i class="fa fa-forward"></i> Ulasan Tempat yang dilaporkan
                            </a>  
                        </div>
                    </div>
                </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->

@endsection