@extends('layouts.admin')
@section('content')

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Data Laporan</strong>
                        </div>
                        <div class="card-body">
                  <table id="bootstrap-data-table" class="table table-striped table-bordered">
                    <thead style="font-size: 15px">
                      <tr>
                        <th>No</th>
                        <th>Waktu Lapor</th>
                        <th>Pelapor</th>
                        <th>Tempat</th>
                        <th>Status</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                      <tbody>
                            @foreach($laporan as $index=>$laporan)
                              <tr>
                                <td>{{$index+1}}</td>
                                <td>{{$laporan->created_at}}</td>
                                <td>{{$laporan->pengunjung_nama}}</td>
                                <td>{{$laporan->tempat_nama}}</td>
                                @if($laporan->laporan_status ==='Verifikasi')
                                    <td class="text-center" ><h5 style="width: 75%" class="badge badge-success"> Terverifikasi </h5></td>
                                @else
                                    <td class="text-center" ><h5 style="width: 75%" class="badge badge-danger"> Belum Terverifikasi </h5></td>
                                @endif
                        <td>
                            <a class="btn btn-outline-primary btn-sm" href="{{ url('/laporan/' . $laporan->id) }}" style="font-size: 12px"><i class="fa fa-list"></i> Detail</a> |
                            <a class="btn btn-outline-danger btn-sm" href="{{ url('/laporan/' . $laporan->id . '/hapus') }}" onclick="return confirm('Apakah Anda yakin menghapus data ?')" style="font-size: 12px"><i class="fa fa-trash"></i> Hapus</a>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                        </div>
                    </div>
                </div>


                </div>
            </div><!-- .animated -->
        </div><!-- .content -->

    <!-- .data tabel -->

    <script src="{{asset('js/lib/data-table/datatables.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/jszip.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/pdfmake.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/vfs_fonts.js')}}"></script>
    <script src="{{asset('js/lib/data-table/buttons.html5.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/buttons.print.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/datatables-init.js')}}"></script>


    <script type="text/javascript">
        $(document).ready(function() {
          $('#bootstrap-data-table-export').DataTable();
        } );
    </script>

@endsection