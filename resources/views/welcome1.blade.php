<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <style>
      html, body {
        margin: 0;
        padding: 0;
        height: 100%;
        width: 100%;
      }
      #map {
        height: 400px;
        width: 100%;
      }
      #legend {
        font-family: Arial, sans-serif;
        background: #fff;
        padding: 10px;
        margin: 10px;
        border: 3px solid #000;
      }
      #legend h3 {
        margin-top: 0;
      }
      #legend img {
        vertical-align: middle;
      }
    </style>

    <!-- ============================================= -->
    
    <!-- ============================================= -->
    <!-- ============================================= -->
    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">
{{--     <link rel="stylesheet" href="{{asset('css/normalize.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/cs-skin-elastic.css')}}">
    <link rel="stylesheet" href="{{asset('scss/style.css')}}"> --}}
    <!-- =============================================== -->

    <title>Near You</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}">

    <!-- Custom fonts for this template -->
    <link rel="stylesheet" href="{{asset('vendor/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Kaushan+Script">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700">
    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="{{asset('css/agency.min.css')}}">

{{-- ==================PETA============================== --}}
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <link rel="stylesheet" href="{{asset('css/demo.css')}}">

  <style>
    #input-img > input {
      display: none;
    }
  </style>
  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">NEAR YOU</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav text-uppercase ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#services">Peta</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">Tambah Tempat Baru</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Header -->
    <header class="masthead">
      <div class="container">
        <div class="intro-text">
          <div class="intro-lead-in">Aplikasi rekomendasi tempat terdekat Anda!</div>
          <div class="intro-heading text-uppercase">Near You</div>
          <a class="btn btn-info btn-xl text-uppercase js-scroll-trigger" href="#services">Peta</a>
          <a class="btn btn-secondary btn-xl text-uppercase js-scroll-trigger" href="#contact">Tambah Tempat Baru</a>
        </div>
      </div>
    </header>

    <!-- Services -->
    <section id="services">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Peta</h2>
            <h3 class="section-subheading text-muted">Rekomendasi Tempat terdekar Anda dalam jarak 5km</h3>
              <div id="map"></div>
             
          </div>
        </div>
      </div>
    </section>


    <!-- Contact -->
    <section id="contact">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Rekomendasi Tempat Baru</h2>
            <h3 class="section-subheading text-muted"    style="margin-bottom: 30px">Kontribusi Anda sangat berarti bagi kami</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
                        <form action="{{route('simpan_rekomendasi')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
                          {{csrf_field()}}
                         <div class="col-lg-6">
                        <!-- nama tempat -->
                          <div class="row form-group" {{ $errors->has('tempat_nama') ? 'has-error' : '' }}>
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Nama Tempat</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="tempat_nama" placeholder="Text" class="form-control"><small class="form-text text-muted">ex. RM Padang Murah</small></div>
                            {!! $errors->first('tempat_nama', '<p class="help-block">:message</p>') !!}
                          </div>
                        <!-- deskripsi -->
                          <div class="row form-group" {{ $errors->has('tempat_deskripsi') ? 'has-error' : '' }}>
                            <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Deskripsi Tempat</label></div>
                            <div class="col-12 col-md-9"><textarea name="tempat_deskripsi" id="textarea-input" rows="9" placeholder="Deskripsi" class="form-control"></textarea></div>
                          {!! $errors->first('tempat_deskripsi', '<p class="help-block">:message</p>') !!}
                          </div>
                        <!-- Kategori -->
                          <div  class="row form-group" {{ $errors->has('kategori_id') ? 'has-error' : '' }}>
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Kategori</label></div>
                            <div class="col-12 col-md-9">
                              <select class="form-control" name="kategori_id">
                                  <option value="">-Pilih Kategori-</option>                                  
                                  @foreach($kategori as $data)                                  
                                  <option value="{{ $data->id }}">{{ $data->kategori_nama }}</option>
                                  @endforeach
                              </select>
                            </div>
                            {!! $errors->first('kategori_id', '<p class="help-block">:message</p>') !!}
                        </div>
                        <!-- Input Image -->  
                          <div class="row form-group" {{ $errors->has('gambar_direktori') ? 'has-error' : '' }}>
                            <div class="col col-md-3"><label for="file-input" class=" form-control-label">Masukkan Foto Tempat</label></div>
                            <div class="col-12 col-md-9">
                              <span class="input-group-btn">
                                <!-- image-preview-clear button -->
                                <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                  <span class="glyphicon glyphicon-remove"></span> Clear
                                </button>
                                <!-- image-preview-input -->
                                <div class="btn btn-default image-preview-input">
                                  <span class="glyphicon glyphicon-folder-open"></span>
                                  <span class="image-preview-input-title"></span>
                                  <input type="file" accept="image/png, image/jpeg, image/gif" name="gambar_direktori[]"/ multiple=""> <!-- rename it -->
                                </div>
                              </span>
                            </div>
                            {!! $errors->first('gambar_direktori', '<p class="help-block">:message</p>') !!}
                          </div>
                        <!-- Status -->
                          <div class="row form-group" {{ $errors->has('tempat_status') ? 'has-error' : '' }}>
                            {!! $errors->first('tempat_status', '<p class="help-block">:message</p>') !!}
                          </div>
                        </div>
                        <div class="col-lg-6">
                          <fieldset class="gllpLatlonPicker">
                            <br/><br/>
                            <div class="gllpMap">Google Maps</div>
                            <br/>
                              <!-- Latitude -->
                              <div  class="row form-group" {{ $errors->has('tempat_latitude') ? 'has-error' : '' }}>
                                  <div class="col col-md-3"><label for="text-input" class=" form-control-label">Latitude</label></div>
                                  <div class="col-12 col-md-9"><input type=
                                    "double" id="number-input" name="tempat_latitude" placeholder="" class="gllpLatitude" value="20" ><small class="form-text text-muted"></small></div>
                                  {!! $errors->first('tempat_latitude', '<p class="help-block">:message</p>') !!}
                              </div>
                              <!-- longitude -->
                              <div class="row form-group" {{ $errors->has('tempat_longitude') ? 'has-error' : '' }}>
                                <div class="col col-md-3"><label for="text-input" class=" form-control-label">Longitude</label></div>
                                <div class="col-12 col-md-9"><input type="double" id="number-input" name="tempat_longitude" placeholder="" class="gllpLongitude" value="20"><small class="form-text text-muted"></small></div>
                                {!! $errors->first('tempat_longitude', '<p class="help-block">:message</p>') !!}
                              </div>
                              {{-- zoom --}}
                              <div class="row form-group" >
                                <div class="col col-md-3"><label class=" form-control-label">Zoom</label></div>
                                <div class="col-12 col-md-9"><input type="number" id="number-input" name="zoom" placeholder="" class="gllpZoom" value="3"><small class="form-text text-muted"></small></div>
                              </div>
                            <input type="button" class="gllpUpdateButton" value="update map">
                            <br/>
                          </fieldset>
                        </div>
                          <div class="card-footer">
                            <button type="submit" class="btn btn-primary btn-sm">
                              <i class="fa fa-dot-circle-o"></i> Submit
                            </button>
                            <button type="reset" class="btn btn-warning btn-sm"><a href="{{ url('/ulasan') }}">
                              <i class="fa fa-outline"></i> Kembali
                            </button>
                        </div>
                        </form>
                       </div>
        </div>
      </div>
    </section>

    <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
                  <h2 class="text-uppercase">Project Name</h2>
                  <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                  <img class="img-fluid d-block mx-auto" src="img/portfolio/01-full.jpg" alt="">
                  <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                  <ul class="list-inline">
                    <li>Date: January 2017</li>
                    <li>Client: Threads</li>
                    <li>Category: Illustration</li>
                  </ul>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    Close Project</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    
    <!-- Plugin JavaScript -->
    <script src="{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>

    <!-- Contact form JavaScript -->
    <script src="{{asset('js/jqBootstrapValidation.js')}}"></script>
    <script src="{{asset('js/contact_me.js')}}"></script>

    <!-- Custom scripts for this template -->
    <script src="{{asset('js/agency.min.js')}}"></script>

    <!-- ========================================= -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js')}}"></script>
    <script src="{{asset('js/plugins.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('js/lib/chart-js/Chart.bundle.js')}}"></script>
    <script src="{{asset('js/dashboard.js')}}"></script>
    <script src="{{asset('js/widgets.js')}}"></script>
    <script src="{{asset('js/lib/vector-map/jquery.vmap.js')}}"></script>
    <script src="{{asset('js/lib/vector-map/jquery.vmap.min.js')}}"></script>
    <script src="{{asset('js/lib/vector-map/jquery.vmap.sampledata.js')}}"></script>
    <script src="{{asset('js/lib/vector-map/country/jquery.vmap.world.js')}}"></script>
    <!-- ========================================= -->
      <script async defer
              src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA_jUF9N0zsKeeZ-t9PtnmdhZlE0AInG_c&callback=initMap">
              </script>
                                 <script>
                var map;
                function initMap() {
                  map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 16,
                    center: new google.maps.LatLng(-33.91722, 151.23064),
                    mapTypeId: 'roadmap'
                  });

                  var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
                  var icons = {
                    @foreach($kategori as $data)
                      {{$data->id}} : {
                        name : {{$data->kategori_nama}},
                        icon : iconBase + {{$data->kategori_icon}}
                      }
                    @endforeach
                  };

                  var features = [
                    @foreach($ulasan as $ulasan)
                      {
                        position new  google.maps.LatLng({{$ulasan->tempat_latitude}}, {{$ulasan->tempat_longitude}}), type: {{$ulasan->kategori_id}}
                      }
                    @endforeach
                  ];

                  // Create markers.
                  features.forEach(function(feature) {
                    var marker = new google.maps.Marker({
                      position: feature.position,
                      icon: icons[feature.type].icon,
                      map: map
                    });
                  });

                  var legend = document.getElementById('legend');
                  for (var key in icons) {
                    var type = icons[key];
                    var name = type.name;
                    var icon = type.icon;
                    var div = document.createElement('div');
                    div.innerHTML = '<img src="' + icon + '"> ' + name;
                    legend.appendChild(div);
                  }

                  map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(legend);
                }
              </script>
            

  </body>
</html>
