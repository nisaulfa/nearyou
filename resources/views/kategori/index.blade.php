@extends('layouts.admin')
@section('content')

        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Tabel Kategori</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                   <a class="btn btn-primary btn-sm" href="{{ url('/tambah_kategori') }}" style="margin-top: 10px" ><i class="fa fa-plus"></i> Tambah Kategori</a>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Data Table</strong>
                        </div>
                        <div class="card-body">
                  <table id="bootstrap-data-table" class="table table-striped table-bordered">
                    <thead style="font-size: 15px">
                      <tr>
                        <th>No</th>
                        <th>Kategori</th>
                        <th>Ikon</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                      <tbody style="font-size: 13px">
                            @foreach($kategori as $index=>$kategori)
                              <tr>
                                <td>{{$index+1}}</td>
                                <td>{{$kategori->kategori_nama}}</td>
                                <td><img src="{{asset('icon_kategori/'.$kategori->kategori_icon)}}" width="50px" height="50px"></td>
                        <td>
                            <a class="btn btn-outline-warning btn-sm" href="{{ url('/kategori/' . $kategori->id . '/edit') }}" style="font-size: 12px"><i class="fa fa-pencil"></i> Ubah</a> |
                            <a class="btn btn-outline-danger btn-sm" type="submit" href="{{ url('/kategori/' . $kategori->id . '/hapus') }}" onclick="return confirm('Apakah Anda yakin menghapus data ?')" style="font-size: 12px"><i class="fa fa-trash"></i> Hapus</a>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                        </div>
                    </div>
                </div>


                </div>
            </div><!-- .animated -->
        </div><!-- .content -->

    <!-- .data tabel -->

    <script src="{{asset('js/lib/data-table/datatables.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/jszip.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/pdfmake.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/vfs_fonts.js')}}"></script>
    <script src="{{asset('js/lib/data-table/buttons.html5.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/buttons.print.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('js/lib/data-table/datatables-init.js')}}"></script>


    <script type="text/javascript">
        $(document).ready(function() {
          $('#bootstrap-data-table-export').DataTable();
        } );
    </script>

@endsection