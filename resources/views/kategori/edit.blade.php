
@extends('layouts.admin')

@section('content')
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Kategori Tempat</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="{{ url('/home') }}">Dashboard</a></li>
                            <li><a href="{{ url('/kategori') }}">Kategori Tempat</a></li>
                            <li class="active">Ubah Kategroi Tempat</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">


                <div class="row">
                  <div class="col-sm-2"></div>
                  <div class="col-lg-8">
                    <div class="card">
                      <div class="card-header">
                        <strong>Ubah Kategroi Tempat</strong>
                      </div>
                      <div class="card-body card-block">
                        <form action="{{url()->current()}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                          {{csrf_field()}}
                        <!-- nama kategori -->
                          <div class="row form-group">
                            <div class="col col-md-4"><label for="text-input" class=" form-control-label">Nama Kategori</label></div>
                            <div class="col-12 col-md-8"><input type="text" id="text-input" name="kategori_nama" placeholder="Nama Kategori" class="form-control" value="{{$kategori->kategori_nama}}"><small class="form-text text-muted">Masukkan nama kategori</small></div>
                          </div>
                                                 <!-- Input Image -->  
                          <div class="row form-group">
                            <div class="col col-md-4"><label for="file-input" class=" form-control-label">Masukkan Foto Tempat</label></div>
                            <div class="col-12 col-md-8">
                              <span class="input-group-btn">
                                <!-- image-preview-clear button -->
                                <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                  <span class="glyphicon glyphicon-remove"></span> Clear
                                </button>
                                <!-- image-preview-input -->
                                <div class="btn btn-default image-preview-input">
                                  <span class="glyphicon glyphicon-folder-open"></span>
                                  <span class="image-preview-input-title"></span>
                                  <input type="file" accept="image/png, image/jpeg, image/jpg" name="kategori_icon"/> <!-- rename it -->
                                </div>
                              </span>
                            </div>
                          </div>
                            <div class="card-footer">
                              <button type="submit" class="btn btn-primary btn-sm">
                                <i class="fa fa-dot-circle-o"></i> Update
                              </button>
                              <button type="reset" class="btn btn-warning btn-sm"><a href="{{ url('/kategori') }}">
                              <i class="fa fa-outline"></i> Kembali
                            </button>
                          </div>
                        </form>
                      </div>
                   </div><!-- .card -->

                </div><!-- .col -->

                </div><!-- .row -->
            </div><!-- .animated -->
        </div><!-- .content -->
@endsection