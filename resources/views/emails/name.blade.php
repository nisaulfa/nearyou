<div class="container-email">
	<div class="row-email">
        @if($ulasan->keterangan ==='Ulasan')
		    <h2>Hai, {{$ulasan->pengunjung_nama}}</h2>
		    <p>Kami selaku administrator dari Aplikasi Near You mengucapkapkan terima kasih atas kontribusi kamu dalam memberikan rekomendasi untuk</p>
		    <ul>
		    	<li>Nama tempat : <b>{{$ulasan->tempat_nama}}</b></li>
		    	<li>Alamat : <b>{{$ulasan->tempat_alamat}}</b></li>
		    	<li>Nomor telepon : <b>{{$ulasan->tempat_telepon}}</b></li>
		    	<li>Deskripsi : <b>{{$ulasan->tempat_deskripsi}}</b></li>
		    </ul>
		    <p>Ayo cek tempat yang kamu rekomendasikan di website Near You</p>
		    <p>Best Regard</p>
		    <p>Administrator Near You</p>
        @else
		    <h2>Hai, {{$ulasan->pengunjung_nama}}</h2>
		    <p>Kami selaku administrator dari Aplikasi Near You mengucapkapkan terima kasih atas kontribusi kamu dalam memberikan laporan untuk</p>
		    <ul>
		    	<li>Nama tempat : <b>{{$ulasan->tempat_nama}}</b></li>
		    	<li>Alamat : <b>{{$ulasan->tempat_alamat}}</b></li>
		    	<li>Nomor telepon : <b>{{$ulasan->tempat_telepon}}</b></li>
		    	<li>Deskripsi : <b>{{$ulasan->tempat_deskripsi}}</b></li>
		    </ul>
		    <p>Kami tunggu kontribusi kamu lagi di website Near You</p>
		    <p>Best Regard</p>
		    <p>Administrator Near You</p>
        @endif




	</div>
</div>