@extends('layouts.admin')

@section('content')
{{-- style maps --}}

        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h2>Dashboard</h2>
                    </div>
                </div>
            </div>
        </div>


        <div class="content mt-4">
            <div class="col-sm-6 col-lg-4">
                <div class="card">
                    <div class="card-body text-white bg-flat-color-1">
                        <div class="stat-widget-one">
                            <div class="stat-icon dib"><i class="ti-map-alt text border"></i></div>
                            <div class="stat-content dib">
                                <div class="text-light">Jumlah Ulasan Tempat</div>
                                <h4 class="mb-0">
                                    <span class="count">{{$ulasan}}</span>
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.col-->
            <div class="col-sm-6 col-lg-4">
                <div class="card">
                    <div class="card-body text-white bg-flat-color-4">
                        <div class="stat-widget-one">
                            <div class="stat-icon dib"><i class="ti-check-box text border"></i></div>
                            <div class="stat-content dib">
                                <div class="text-light">Ulasan Belum Verifikasi</div>
                                <h4 class="mb-0">
                                    <span class="count">{{$belumverif}}</span>
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.col-->

            <div class="col-sm-6 col-lg-4">
                <div class="card">
                    <div class="card-body text-white bg-flat-color-3">
                        <div class="stat-widget-one">
                            <div class="stat-icon dib"><i class="ti-alert text border"></i></div>
                            <div class="stat-content dib">
                                <div class="text-light">Laporan Ulasan Masuk</div>
                                <h4 class="mb-0">
                                    <span class="count">{{$laporan}}</span>
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.col-->
        </div> <!-- .content -->
@endsection
