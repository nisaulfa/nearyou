@extends('layouts.visitor')
@section('contentFronfEnd')

    <nav class="navbar navbar-expand-lg navbar-dark bg-primary" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="{{url('/')}}">NEAR YOU</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
      </div>
    </nav>
    <!-- Services -->
    <section id="services">
      <div class="container">
        <div class="row">
          <div class="col-12 text-center">
            <h2 class="section-heading text-uppercase">{{$data->tempat_nama}}</h2>
          </div>
          <div class="col-10">
                    <div class="card">
                      <div class="card-header">
                        <strong>Laporkan Tempat</strong>
                      </div>
                      <div class="card-body card-block">
                        <form action="{{url()->current()}}" method="post" class="form-horizontal" enctype="multipart/form-data">
                          {{csrf_field()}}
                        <!-- nama tempat -->
                      </br>
                          <div class="row form-group" {{ $errors->has('laporan_nama') ? 'has-error' : '' }}>
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Nama</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="laporan_nama" placeholder="Nama Pelapor" class="form-control"></div>
                            {!! $errors->first('laporan_nama', '<p class="help-block">:message</p>') !!}
                          </div>
                      <!-- nama tempat -->
                      </br>
                          <div class="row form-group" {{ $errors->has('laporan_email') ? 'has-error' : '' }}>
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Email</label></div>
                            <div class="col-12 col-md-9"><input type="email" id="text-input" name="laporan_email" placeholder="Email Pelapor" class="form-control"></div>
                            {!! $errors->first('laporan_email', '<p class="help-block">:message</p>') !!}
                          </div>
                        <!-- deskripsi -->
                          <div class="row form-group" {{ $errors->has('laporan_deskripsi') ? 'has-error' : '' }}>
                            <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Deskripsi Laporant</label></div>
                            <div class="col-12 col-md-9"><textarea name="laporan_deskripsi" id="textarea-input" rows="9" placeholder="" class="form-control"></textarea></div>
                          {!! $errors->first('laporan_deskripsi', '<p class="help-block">:message</p>') !!}
                          </div>

                          <div class="card-footer">
                            <button type="submit" class="btn btn-primary btn-sm">
                              <i class="fa fa-dot-circle-o"></i> Submit
                            </button>
                            <button type="reset" class="btn btn-warning btn-sm"><a href="{{ url('/detail_sample') }}">
                              <i class="fa fa-outline"></i> Kembali
                            </button>
                        </div>
                        </form>
                      </div>
                        
                   </div><!-- .card -->
          </div>
        </div>
      </div>
    </section>

@endsection